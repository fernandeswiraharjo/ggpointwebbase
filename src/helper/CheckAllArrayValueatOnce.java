package helper;

import java.util.List;

public class CheckAllArrayValueatOnce {

	public static boolean allMatch(String[] array, String value) {
	    for (int i = 0; i < array.length; i++)
	    {
	        if ( !(array[i].contentEquals(value)) )
	            return false;
	    }
	    return true;
	}
	
}
