package model;

public class mdlArea {

	public String AreaID;
	public String AreaName;
	public String AreaDescription;
	
	public String getAreaID() {
		return AreaID;
	}
	public void setAreaID(String areaID) {
		AreaID = areaID;
	}
	public String getAreaName() {
		return AreaName;
	}
	public void setAreaName(String areaName) {
		AreaName = areaName;
	}
	public String getAreaDescription() {
		return AreaDescription;
	}
	public void setAreaDescription(String areaDescription) {
		AreaDescription = areaDescription;
	}
	
	
}
