package model;

public class mdlBank {

	public String BankID;
	public String BankName;
	public String BankDescription;
	
	public String getBankID() {
		return BankID;
	}
	public void setBankID(String bankID) {
		BankID = bankID;
	}
	public String getBankName() {
		return BankName;
	}
	public void setBankName(String bankName) {
		BankName = bankName;
	}
	public String getBankDescription() {
		return BankDescription;
	}
	public void setBankDescription(String bankDescription) {
		BankDescription = bankDescription;
	}
	
}
