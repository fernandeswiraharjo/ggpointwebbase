package model;

public class mdlTransactionPayment {

	public String PaymentID;
	public String PaymentDate;
	public String TransactionID;
	public String MerchantID;
	public String MerchantName;
	public String MerchantPIC;
	public String MerchantArea;
	public String TransactionDate;
	public String TransactionPrice;
	public Integer PaymentAmount;
	public String sPaymentAmount;
	public String Status;
	public String RefNo;
	public String CreatedBy;
	public String LastUpdateBy;
	public String VerifiedBy;
	public String CreatedDate;
	public String LastDate;
	public String VerifiedDate;
	public String ButtonStatus;
	public String ButtonStatusReject;
	public String MerchantUserName;
//	public Integer TotalTransaction;
	
	
//	public Integer getTotalTransaction() {
//		return TotalTransaction;
//	}
//	public void setTotalTransaction(Integer totalTransaction) {
//		TotalTransaction = totalTransaction;
//	}
	public String getPaymentID() {
		return PaymentID;
	}
	public void setPaymentID(String paymentID) {
		PaymentID = paymentID;
	}
	public String getPaymentDate() {
		return PaymentDate;
	}
	public void setPaymentDate(String paymentDate) {
		PaymentDate = paymentDate;
	}
	public String getTransactionID() {
		return TransactionID;
	}
	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}
	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getMerchantName() {
		return MerchantName;
	}
	public void setMerchantName(String merchantName) {
		MerchantName = merchantName;
	}
	public String getMerchantPIC() {
		return MerchantPIC;
	}
	public void setMerchantPIC(String merchantPIC) {
		MerchantPIC = merchantPIC;
	}
	public String getMerchantArea() {
		return MerchantArea;
	}
	public void setMerchantArea(String merchantArea) {
		MerchantArea = merchantArea;
	}
	public String getTransactionDate() {
		return TransactionDate;
	}
	public void setTransactionDate(String transactionDate) {
		TransactionDate = transactionDate;
	}
	public String getTransactionPrice() {
		return TransactionPrice;
	}
	public void setTransactionPrice(String transactionPrice) {
		TransactionPrice = transactionPrice;
	}
	public Integer getPaymentAmount() {
		return PaymentAmount;
	}
	public void setPaymentAmount(Integer paymentAmount) {
		PaymentAmount = paymentAmount;
	}
	public String getsPaymentAmount() {
		return sPaymentAmount;
	}
	public void setsPaymentAmount(String sPaymentAmount) {
		this.sPaymentAmount = sPaymentAmount;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getRefNo() {
		return RefNo;
	}
	public void setRefNo(String refNo) {
		RefNo = refNo;
	}
	public String getCreatedBy() {
		return CreatedBy;
	}
	public void setCreatedBy(String createdBy) {
		CreatedBy = createdBy;
	}
	public String getLastUpdateBy() {
		return LastUpdateBy;
	}
	public void setLastUpdateBy(String lastUpdateBy) {
		LastUpdateBy = lastUpdateBy;
	}
	public String getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}
	public String getLastDate() {
		return LastDate;
	}
	public void setLastDate(String lastDate) {
		LastDate = lastDate;
	}
	public String getButtonStatus() {
		return ButtonStatus;
	}
	public void setButtonStatus(String buttonStatus) {
		ButtonStatus = buttonStatus;
	}
	public String getButtonStatusReject() {
		return ButtonStatusReject;
	}
	public void setButtonStatusReject(String buttonStatusReject) {
		ButtonStatusReject = buttonStatusReject;
	}
	public String getVerifiedBy() {
		return VerifiedBy;
	}
	public void setVerifiedBy(String verifiedBy) {
		VerifiedBy = verifiedBy;
	}
	public String getVerifiedDate() {
		return VerifiedDate;
	}
	public void setVerifiedDate(String verifiedDate) {
		VerifiedDate = verifiedDate;
	}
	public String getMerchantUserName() {
		return MerchantUserName;
	}
	public void setMerchantUserName(String merchantUserName) {
		MerchantUserName = merchantUserName;
	}
	
	
}
