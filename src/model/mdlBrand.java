package model;

public class mdlBrand {

	public String BrandID;
	public String BrandName;
	public String BrandDescription;
	public String getBrandID() {
		return BrandID;
	}
	public void setBrandID(String brandID) {
		BrandID = brandID;
	}
	public String getBrandName() {
		return BrandName;
	}
	public void setBrandName(String brandName) {
		BrandName = brandName;
	}
	public String getBrandDescription() {
		return BrandDescription;
	}
	public void setBrandDescription(String brandDescription) {
		BrandDescription = brandDescription;
	}

}
