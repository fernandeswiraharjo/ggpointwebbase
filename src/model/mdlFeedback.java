package model;

public class mdlFeedback {
	public String FeedbackID;
	public String File;
	public String Date;
	public String Description;
	public String CreatedDate;
	public String Created_by;
	public String LastDate;
	public String LastUpdateBy;
	public String Status;
	public String Type;
	public String MerchantID;
	public String MerchantName;
	public String PIC;
	public String Username;
	public String ContestReason;
	
	public String getPIC() {
		return PIC;
	}
	public void setPIC(String pIC) {
		PIC = pIC;
	}
	public String getUsername() {
		return Username;
	}
	public void setUsername(String username) {
		Username = username;
	}
	public String getFeedbackID() {
		return FeedbackID;
	}
	public void setFeedbackID(String feedbackID) {
		FeedbackID = feedbackID;
	}
	public String getFile() {
		return File;
	}
	public void setFile(String file) {
		File = file;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getCreatedDate() {
		return CreatedDate;
	}
	public void setCreatedDate(String createdDate) {
		CreatedDate = createdDate;
	}
	public String getCreated_by() {
		return Created_by;
	}
	public void setCreated_by(String created_by) {
		Created_by = created_by;
	}
	public String getLastDate() {
		return LastDate;
	}
	public void setLastDate(String lastDate) {
		LastDate = lastDate;
	}
	public String getLastUpdateBy() {
		return LastUpdateBy;
	}
	public void setLastUpdateBy(String lastUpdateBy) {
		LastUpdateBy = lastUpdateBy;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getMerchantName() {
		return MerchantName;
	}
	public void setMerchantName(String merchantName) {
		MerchantName = merchantName;
	}
	public String getContestReason() {
		return ContestReason;
	}
	public void setContestReason(String contestReason) {
		ContestReason = contestReason;
	}
	
}
