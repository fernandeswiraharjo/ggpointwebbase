package model;

public class mdlDistrictDetail {

	public String id;
	public String nama;
	public String id_prov;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getId_prov() {
		return id_prov;
	}
	public void setId_prov(String id_prov) {
		this.id_prov = id_prov;
	}
	
}
