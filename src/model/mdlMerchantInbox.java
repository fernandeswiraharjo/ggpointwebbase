package model;

public class mdlMerchantInbox {

	public String MessageID;
	public String Title;
	public String Date;
	public String Subject;
	public String Message;
	public String Status;
	public String StatusDetail;
	public String MerchantID;
	public String MerchantName;
	public String MerchantPIC;
	public String TransactionID;
	public Integer TransactionPrice;
	public String RefNo;
	public String MessageType;
	public String IssuedBy;
	public String IssuedDate;
	public String ContestReason;
	public String ContestFile;
	public String UserName;
	
	public String getMessageID() {
		return MessageID;
	}
	public void setMessageID(String messageID) {
		MessageID = messageID;
	}
	public String getTitle() {
		return Title;
	}
	public void setTitle(String title) {
		Title = title;
	}
	public String getDate() {
		return Date;
	}
	public void setDate(String date) {
		Date = date;
	}
	public String getSubject() {
		return Subject;
	}
	public void setSubject(String subject) {
		Subject = subject;
	}
	public String getMessage() {
		return Message;
	}
	public void setMessage(String message) {
		Message = message;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getStatusDetail() {
		return StatusDetail;
	}
	public void setStatusDetail(String statusDetail) {
		StatusDetail = statusDetail;
	}
	public String getMerchantID() {
		return MerchantID;
	}
	public void setMerchantID(String merchantID) {
		MerchantID = merchantID;
	}
	public String getMerchantName() {
		return MerchantName;
	}
	public void setMerchantName(String merchantName) {
		MerchantName = merchantName;
	}
	public String getMerchantPIC() {
		return MerchantPIC;
	}
	public void setMerchantPIC(String merchantPIC) {
		MerchantPIC = merchantPIC;
	}
	public String getTransactionID() {
		return TransactionID;
	}
	public void setTransactionID(String transactionID) {
		TransactionID = transactionID;
	}
	public Integer getTransactionPrice() {
		return TransactionPrice;
	}
	public void setTransactionPrice(Integer transactionPrice) {
		TransactionPrice = transactionPrice;
	}
	public String getIssuedBy() {
		return IssuedBy;
	}
	public void setIssuedBy(String issuedBy) {
		IssuedBy = issuedBy;
	}
	public String getIssuedDate() {
		return IssuedDate;
	}
	public void setIssuedDate(String issuedDate) {
		IssuedDate = issuedDate;
	}
	public String getRefNo() {
		return RefNo;
	}
	public void setRefNo(String refNo) {
		RefNo = refNo;
	}
	public String getMessageType() {
		return MessageType;
	}
	public void setMessageType(String messageType) {
		MessageType = messageType;
	}
	public String getContestReason() {
		return ContestReason;
	}
	public void setContestReason(String contestReason) {
		ContestReason = contestReason;
	}
	public String getContestFile() {
		return ContestFile;
	}
	public void setContestFile(String contestFile) {
		ContestFile = contestFile;
	}
	public String getUserName() {
		return UserName;
	}
	public void setUserName(String userName) {
		UserName = userName;
	}
	
	
}
