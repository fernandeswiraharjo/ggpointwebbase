package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.eclipse.jdt.internal.compiler.ast.ThrowStatement;

import com.sun.rowset.JdbcRowSetImpl;
import com.sun.rowset.RowSetFactoryImpl;

/** Documentation
 * 
 */
public class RowSetAdapter {
	
	private static Connection conn;
	
	public static JdbcRowSet getJDBCRowSet() throws Exception{
		InitialContext context = new InitialContext();
    	DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/gg_merchant");
    	conn = dataSource.getConnection();
    	JdbcRowSetImpl jdbcRs = new JdbcRowSetImpl(conn);

    	return jdbcRs;
	}
		 
	public static Connection getConnection()throws Exception{
		 
			 try {
			     InitialContext context = new InitialContext();
			     DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/gg_merchant");
			    		 
			     conn = dataSource.getConnection();
			      
			 } 
			 catch (Exception e) {
		            e.printStackTrace();
		     }
			 
		     
		     return conn;
	}

}
