package com.ggmerchant;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.LogAdapter;
import adapter.LoginAdapter;
import adapter.UserAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/login"} , name="login")
public class LoginServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public LoginServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/login.jsp");
		dispacther.forward(request, response);
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
    	String lUserId = "";
    	String lUserType = "";
    	String lUserName = request.getParameter("username");  
        String lPassword = request.getParameter("password"); 
        String btnLogin = request.getParameter("btnLogin");
        
        if (btnLogin != null)
        {   
			if(LoginAdapter.ValidasiLogin(lUserName, lPassword)) {
				lUserId = UserAdapter.LoadUserId(lUserName, lPassword);
				lUserType = UserAdapter.LoadUserType(lUserName, lPassword);
				
				HttpSession session = request.getSession();
				session.setAttribute("user", lUserId);
				session.setAttribute("usertype", lUserType);
//				session.setAttribute("password", lPassword);
				//setting session to expiry in 30 mins
				session.setMaxInactiveInterval(30*60);
				
				//RequestDispatcher rd=request.getRequestDispatcher("dashboard");
				//rd.forward(request,response);
			    response.sendRedirect(request.getContextPath() + "/dashboard");
			}  
			else {
				request.setAttribute("condition", "1"); 
				RequestDispatcher rd=request.getRequestDispatcher("/mainform/pages/login.jsp");
			    rd.forward(request,response);
			}
				
        	return;
        }
	}
    
}
