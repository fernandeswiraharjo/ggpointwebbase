package com.ggmerchant;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.DateRangeAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.UserAdapter;
import adapter.ValidateNull;
import helper.ConvertDateTimeHelper;
import model.Globals;
import net.sf.jasperreports.engine.JRPrintPage;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;

@WebServlet(urlPatterns={"/report_merchant"} , name="report_merchant")
public class RptMerchantServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
	public RptMerchantServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	String result,resultDesc;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		Boolean CheckMenu;
    	String MenuURL = "report_merchant";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
    		AreaList.addAll(AreaAdapter.LoadAreaforReport(mdlUser));
    		request.setAttribute("listarea", AreaList);
    		
			request.setAttribute("condition", result);
    		
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/rpt_merchant.jsp");
    		dispacther.forward(request, response);
    	}
		
    	result = "";
    	resultDesc = "";
	}
	
	@SuppressWarnings("deprecation")
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		model.mdlUser mdlUser = new model.mdlUser();
		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
		mdlUser.setUserId(user);
		
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    	
    	
		//Declare button
		String keyBtn = request.getParameter("txtExportType");
		if (keyBtn == null){
			keyBtn = new String("");
			
			return;
		}
		
		//Declare connection
		Connection connection = null;
		try {
			//Declare Parameter
			connection = database.RowSetAdapter.getConnection();
			
			String rAreaID = request.getParameter("slArea");
			String rAreasID = mdlUser.Area;
			String rBrandID = mdlUser.Brand;
			String rdateNow = ConvertDateTimeHelper.formatDate(LocalDateTime.now().toString(), "yyyy-MM-dd", "dd MMM yyyy");
			//Globals.gCommand = rMerchantName;
			
			JasperPrint jasperPrint = null;
			JasperReport jasperReport;
			String paramJasperReport = getServletContext().getInitParameter("param_jasper_report");
			String paramJasperPrint = getServletContext().getInitParameter("param_jasper_print");
			String paramLogoPath = getServletContext().getInitParameter("param_logo");
			
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("lreportdate", ValidateNull.NulltoStringEmpty(rdateNow));
			
			params.put("lareaid", ValidateNull.NulltoStringEmpty("'%"+rAreaID+"%'"));
			params.put("lareasid", ValidateNull.NulltoStringEmpty(rAreasID));
			params.put("lbrandsid", ValidateNull.NulltoStringEmpty(rBrandID));
			params.put("limage", ValidateNull.NulltoStringEmpty(paramLogoPath));
			
			jasperReport = JasperCompileManager.compileReport(paramJasperReport+"rpt_merchant.jrxml");
			jasperPrint = JasperFillManager.fillReport(jasperReport, params,connection);
			//check page is blank or not
			List<JRPrintPage> pages = jasperPrint.getPages();
		    if (pages.size()==0){
		        //No pages, do not export instead do other stuff
				//		    	Alert alert = new Alert(AlertType.INFORMATION);
				//		        alert.setTitle("a");
				//		        alert.setHeaderText("b");
				//		        alert.setContentText("c");
				//		        alert.showAndWait();
		    	PrintWriter out = response.getWriter();  
		    	response.setContentType("text/html");  
		    	out.println("<script type=\"text/javascript\">");  
		    	out.println("alert('NO AVAILABLE DATA');");  
		    	out.println("</script>");
		    }
		    else {
		    	//-- export process --
				if (keyBtn.equals("pdf")){
					//coding for exporting to Pdf
					JasperExportManager.exportReportToPdfFile(jasperPrint, paramJasperPrint+"rpt_merchant.pdf");
					
					String pdfFileName = "rpt_merchant.pdf";
					File pdfFile = new File(paramJasperPrint+"rpt_merchant.pdf");
					
					response.setContentType("application/pdf");
					response.addHeader("Content-Disposition: inline;", "filename=" + pdfFileName);
					response.setContentLength((int) pdfFile.length());
					FileInputStream fileInputStream = new FileInputStream(pdfFile);
					OutputStream responseOutputStream = response.getOutputStream();
					int bytes;
					
					while ((bytes = fileInputStream.read()) != -1) {
						responseOutputStream.write(bytes);
					}
				}
				else if(keyBtn.equals("excel")){
					//coding for Excel
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					JRXlsExporter xlsExporter = new JRXlsExporter();
		            xlsExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
		            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, os);
		            xlsExporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, "rpt_merchant.xls");
		            xlsExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
		            xlsExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		            xlsExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		            xlsExporter.exportReport();
		            
		            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		            response.setHeader("Content-Disposition", "attachment; filename=rpt_merchant.xls");
		            
		            //uncomment this codes if u are want to use servlet output stream
		            //servletOutputStream.write(os.toByteArray());
		            
		            response.getOutputStream().write(os.toByteArray());
		            response.getOutputStream().flush();
		            response.getOutputStream().close();
		            response.flushBuffer();
				}
		    }
		}
		catch(Exception ex)
		{
			LogAdapter.InsertLogExc(ex.toString(), "EXPORT REPORT DATA MERCHANT", "", user);
			//Globals.gCondition = "FailedExport";
			//Globals.gConditionDesc = "Please try again or contact admin for further help";
			
			//doGet(request, response);
		}
		finally{
			try {
				//close the opened connection
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "EXPORT REPORT DATA MERCHANT", "close opened connection protocol", user);
			}
		}
		//-- end of export process --
        
        return;
	}
	
}
