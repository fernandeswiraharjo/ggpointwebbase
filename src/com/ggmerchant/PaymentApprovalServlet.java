package com.ggmerchant;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.AreaAdapter;
import adapter.BankAdapter;
import adapter.BrandAdapter;
import adapter.IssueAdapter;
import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.ProvinceAdapter;
import adapter.TransactionAdapter;
import adapter.UserAdapter;
import helper.ConvertDateTimeHelper;
import model.Globals;

@WebServlet(urlPatterns={"/payment_approvall"} , name="payment_approvall")
public class PaymentApprovalServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public PaymentApprovalServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    String BrandID="",AreaID="",tempBrandID="",tempAreaID="",result="",resultDesc="";
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	Boolean CheckMenu;
    	String MenuURL = "payment_approval";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		List<model.mdlTransaction> TransactionList = new ArrayList<model.mdlTransaction>();
        	if(BrandID.contentEquals("") && AreaID.contentEquals(""))
        		request.setAttribute("listtransaction", TransactionList);
        	else
        	{
        		TransactionList.addAll(TransactionAdapter.LoadTransaction(BrandID,AreaID,mdlUser));
        		request.setAttribute("listtransaction", TransactionList);
        	}
    		
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrand(mdlUser));
    		request.setAttribute("listbrand", BrandList);
    		
    		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
    		AreaList.addAll(AreaAdapter.LoadArea(mdlUser));
    		request.setAttribute("listarea", AreaList);
    		
    		String ButtonStatus;
    		ButtonStatus = MenuAdapter.SetMenuButtonStatus(MenuURL, user);
    		if(ButtonStatus.contentEquals("disabled"))
    			ButtonStatus = "none";
    		else
    			ButtonStatus = "";
    		request.setAttribute("buttonstatus", ButtonStatus);
    		
    		request.setAttribute("selectedbrand", BrandID);
    		request.setAttribute("selectedarea", AreaID);
    		request.setAttribute("condition", result);
    		request.setAttribute("errorDescription", resultDesc);
    		
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/payment_approval.jsp");
    		dispacther.forward(request, response);
    		
    		tempBrandID=BrandID;
    		tempAreaID=AreaID;
    		BrandID="";
    		AreaID="";
    	}
		
		result = "";
		resultDesc = "";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    	
		//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		if(keyBtn.equals("validatepayamount")) {
			Integer PayAmount = Integer.parseInt(request.getParameter("payamount").replace(".", ""));
			Integer Price = Integer.parseInt(request.getParameter("price").replace("Rp. ", "").replace(".", "").replace(" ,-", ""));
			String Validate = "";
			
			if( PayAmount > Price )
	    		Validate = "disallow";
	    	else
	    		Validate = "allow";
	    	
	    	response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(Validate);
		}
		
		if(keyBtn.equals("pay")) {
			Integer PayAmount = Integer.parseInt(request.getParameter("payamount").replace(".", ""));
			String PaymentDate = LocalDateTime.now().toString();
			String PaymentID = "GGPoint_Redeem"+ConvertDateTimeHelper.formatDate(PaymentDate, "yyyy-MM-dd'T'HH:mm:ss.SSS", "ddMMyyyyHHmmss");
			String TransactionID = request.getParameter("transactionid");
			String Status = "Waiting";
			String RefNo = "";
			
			//Declare mdlTransactionPayment for global
			model.mdlTransactionPayment TransactionPayment = new model.mdlTransactionPayment();
			TransactionPayment.setPaymentID(PaymentID);
			TransactionPayment.setPaymentDate(PaymentDate);
			TransactionPayment.setTransactionID(TransactionID);
			TransactionPayment.setPaymentAmount(PayAmount);
			TransactionPayment.setStatus(Status);
			TransactionPayment.setRefNo(RefNo);
			
			String lResult = "";
			
			lResult = TransactionAdapter.InsertTransactionPayment(TransactionPayment, user);
			
			if(lResult.contains("Success Insert Payment")) {  
				result = "SuccessApproval";
		    }  
		    else {
		        result = "FailedApproval"; 
		        resultDesc = lResult;
		    }
			
			BrandID=tempBrandID;
			AreaID=tempAreaID;
			
		return;
		}
		
		if(keyBtn.equals("sort")) {
			//Declare slBrand and slArea
	    	BrandID = request.getParameter("BrandID");
	    	AreaID = request.getParameter("AreaID");
	    	
	    	return;
		}
		
    }
    
}
