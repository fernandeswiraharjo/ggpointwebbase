package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import adapter.BrandAdapter;
import adapter.FeedbackAdapter;
import adapter.IssueAdapter;
import adapter.LogAdapter;
import adapter.MenuAdapter;
import adapter.MerchantAdapter;
import adapter.UserAdapter;
import model.Globals;
import model.mdlUser;

@WebServlet(urlPatterns={"/feedback"} , name="feedback")
public class FeedbackServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;
    
    public FeedbackServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    String BrandID, result, resultDesc="";
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	Boolean CheckMenu;
    	String MenuURL = "feedback";
    	CheckMenu = MenuAdapter.CheckMenu(MenuURL, user);
    	
    	if(CheckMenu==false)
    	{
    		RequestDispatcher dispacther = request.getRequestDispatcher("/dashboard");
    		dispacther.forward(request, response);
    	}
    	else
    	{
    		model.mdlUser mdlUser = new model.mdlUser();
    		mdlUser = UserAdapter.GetUserAreaAndBrand(user);
    		mdlUser.setUserId(user);
    		
    		//Declare Parameter
        	List<model.mdlFeedback> mdlFeedbackList = new ArrayList<model.mdlFeedback>();
        	if(BrandID == null || BrandID.contentEquals(""))
        		request.setAttribute("listfeedback", mdlFeedbackList);
        	else
        	{
        		mdlFeedbackList.addAll(FeedbackAdapter.LoadFeedback(BrandID, mdlUser));
    			request.setAttribute("listfeedback", mdlFeedbackList);
        	}
    		
//    		if(IssueMerchantInboxList.isEmpty())
//    			request.setAttribute("pagenotif", "No Issue Found");
    		
    		List<model.mdlBrand> BrandList = new ArrayList<model.mdlBrand>();
    		BrandList.addAll(BrandAdapter.LoadBrand(mdlUser));
    		request.setAttribute("listbrand", BrandList);
    		
    		request.setAttribute("selectedbrand", BrandID);
    		request.setAttribute("condition", result);
    		request.setAttribute("errorDescription", resultDesc);
        	
        	RequestDispatcher dispacther = request.getRequestDispatcher("/mainform/pages/feedback.jsp");
    		dispacther.forward(request, response);
    	}
		
		result = "";
		resultDesc = "";
//		BrandID="";
	}
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
    	if (user == null || user == "")
    	{ 
    		return;
    	}
    	
    	//Declare slBrand
    	BrandID = request.getParameter("BrandID");
    	
    	return;
	}
    
}
