package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.FeedbackAdapter;
import adapter.IssueAdapter;
import adapter.LoginAdapter;
import adapter.PaymentAdapter;
import adapter.ProvinceAdapter;
import adapter.TransactionAdapter;
import adapter.UserAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/site_master"} , name="site_master")
public class SiteMasterServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;
    
    public SiteMasterServlet() {	
        super();
        // TODO Auto-generated constructor stub
    }
    
    String[] list_user_area, list_user_brand;
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		//String userType = (String) session.getAttribute("usertype");
		//Globals.password = (String) session.getAttribute("password");
		
		GetUserAreaAndBrandSession(request, response);
		
    	//Declare button
		String keyBtn = request.getParameter("key");
		if (keyBtn == null){
			keyBtn = new String("");
		}
		
		if (keyBtn.equals("IssueNumber")){
			Integer IssueNumber = IssueAdapter.LoadIssueNumber(list_user_brand,list_user_area,user);
	    	
	    	response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(String.valueOf(IssueNumber));
		}
		if (keyBtn.equals("FeedbackNumber")){
			Integer FeedbackNumber = FeedbackAdapter.LoadFeedbackNumber(list_user_brand,list_user_area,user);
	    	
	    	response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(String.valueOf(FeedbackNumber));
		}
		else if (keyBtn.equals("PendingPaymentApprovalNumber")){
			Integer PendingPaymentApprovalNumber = TransactionAdapter.LoadPendingPaymentApprovalNumber(list_user_brand,list_user_area,user);
	    	
	    	response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(String.valueOf(PendingPaymentApprovalNumber));
		}
		else if (keyBtn.equals("PendingApprovalDocNumber")){
			Integer PendingApprovalDocNumber = PaymentAdapter.LoadPendingApprovalDocNumber(list_user_brand,list_user_area,user);
	    	
	    	response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(String.valueOf(PendingApprovalDocNumber));
		}
		else if (keyBtn.equals("PendingVerificationDocNumber")){
			Integer PendingVerificationDocNumber = PaymentAdapter.LoadPendingVerificationDocNumber(list_user_brand,list_user_area,user);
	    	
	    	response.setContentType("application/text");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(String.valueOf(PendingVerificationDocNumber));
		}
		else if (keyBtn.equals("RestrictedMenu")){
	    	List<model.mdlMenu> MenuList = new ArrayList<model.mdlMenu>();
	    	MenuList = UserAdapter.LoadRestrictedMenu(user);
	    	
	    	Gson gson = new Gson();
	    	
	    	String jsonlistMenuID = gson.toJson(MenuList);
	    	
	    	response.setContentType("application/json");
	        response.setCharacterEncoding("UTF-8");
	        response.getWriter().write(jsonlistMenuID);
		}
	}

	public void GetUserAreaAndBrandSession(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		//Globals.user_area = "";
		//Globals.user_brand = "";
        
        //setting session for user area access
		String user_area = UserAdapter.LoadUserArea(user);
		if(user_area.contentEquals("") || user_area==null)
		{
			//request.setAttribute("condition", "2");
			//RequestDispatcher rd=request.getRequestDispatcher("/mainform/pages/master_header.jsp");
			//rd.forward(request,response);
		}
		//String[] list_user_area = user_area.split(",");
		list_user_area = user_area.split(",");
		//		int list_user_area_length = list_user_area.length; //set the length of list
		//		for(int i=0; i<list_user_area_length;i++)
		//		{
		//			if(Globals.user_area.contentEquals("")) //if first index
		//				Globals.user_area = "'"+list_user_area[i]+"'";
		//			else
		//				Globals.user_area += ","+"'"+list_user_area[i]+"'";
		//			//else if((i + 1) == (list_user_area_length)) //if last index
		//			//Globals.user_area += ","+"'"+list_user_area[i];
		//		}
		
		//setting session for user brand access
		String user_brand = UserAdapter.LoadUserBrand(user);
		if(user_brand.contentEquals("") || user_brand==null)
		{
			//request.setAttribute("condition", "3"); 
			//RequestDispatcher rd=request.getRequestDispatcher("/mainform/pages/master_header.jsp");
			//rd.forward(request,response);
		}
		list_user_brand = user_brand.split(",");
		//		//String[] list_user_brand = user_brand.split(",");
		//		int list_user_brand_length = list_user_brand.length; //set the length of list
		//		for(int i=0; i<list_user_brand_length;i++)
		//		{
		//			if(Globals.user_brand.contentEquals("")) //if first index
		//				Globals.user_brand = "'"+list_user_brand[i]+"'";
		//			else
		//				Globals.user_brand += ","+"'"+list_user_brand[i]+"'";
		//		}
	}
    
}
