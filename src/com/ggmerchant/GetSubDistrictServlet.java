package com.ggmerchant;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.gson.Gson;

import adapter.LogAdapter;
import adapter.ProvinceAdapter;
import model.Globals;

@WebServlet(urlPatterns={"/getsubdistrict"} , name="getsubdistrict")
public class GetSubDistrictServlet extends HttpServlet{

	private static final long serialVersionUID = 1L; 
	
	public GetSubDistrictServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		String user = (String) session.getAttribute("user");
		
		String lDistrictID = request.getParameter("districtid");
    	List<model.mdlSubDistrictDetail> SubDistrictList = new ArrayList<model.mdlSubDistrictDetail>();
		SubDistrictList = ProvinceAdapter.LoadSubDistrict(lDistrictID, user);
    	
    	Gson gson = new Gson();
    	
    	String jsonlistSubDistrict = gson.toJson(SubDistrictList);
    	
    	response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(jsonlistSubDistrict);
	}
	
}
