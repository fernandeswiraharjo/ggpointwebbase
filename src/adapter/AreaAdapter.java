package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlUser;

public class AreaAdapter {

	public static List<model.mdlArea> LoadArea(mdlUser pUser) {
		
		//define local variable
		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadArea(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  pUser.Area);
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlArea Area = new model.mdlArea();
				
				Area.setAreaID(jrs.getString("AreaID"));
				Area.setAreaName(jrs.getString("AreaName"));
				
				AreaList.add(Area);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadArea", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadArea", "close opened connection protocol", pUser.UserId);
			}
		}

		return AreaList;
	}
	
	public static List<model.mdlArea> LoadAreaforReport(mdlUser pUser) {
		
		//define local variable
		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		model.mdlArea Area = new model.mdlArea();
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadArea(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  pUser.Area);
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			Area.setAreaID("");
			Area.setAreaName("All");
			
			AreaList.add(Area);
			
			while(jrs.next()){
				Area = new model.mdlArea();
				
				Area.setAreaID(jrs.getString("AreaID"));
				Area.setAreaName(jrs.getString("AreaName"));
				
				AreaList.add(Area);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadAreaforReport", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadAreaforReport", "close opened connection protocol", pUser.UserId);
			}
		}

		return AreaList;
	}
	
	public static List<model.mdlArea> LoadAreaMenuUser(String user) {
		
		//define local variable
		List<model.mdlArea> AreaList = new ArrayList<model.mdlArea>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadAreaMenuUser()}";
			pstm = connection.prepareStatement(sql);
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlArea Area = new model.mdlArea();
				
				Area.setAreaID(jrs.getString("AreaID"));
				Area.setAreaName(jrs.getString("AreaName"));
				
				AreaList.add(Area);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadAreaMenuUser", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadAreaMenuUser", "close opened connection protocol", user);
			}
		}

		return AreaList;
	}

}
