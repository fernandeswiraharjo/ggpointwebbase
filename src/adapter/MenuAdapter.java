package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

public class MenuAdapter {

	public static List<model.mdlMenu> LoadMenu(String user)  {
		Connection connection = null;
		String command = "";
		
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		PreparedStatement pstmLoadMenuLv1 = null;
		ResultSet jrs2 = null;
		
		PreparedStatement pstmLoadMenuLv2 = null;
		ResultSet jrs3 = null;

		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		try{
			connection = database.RowSetAdapter.getConnection();
			String sqlLoadMenu = "{call wb_LoadMenu_lv0()}";
			pstmLoadMenu = connection.prepareStatement(sqlLoadMenu);
			jrs = pstmLoadMenu.executeQuery();
			
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				mdlMenu.setMenuName(jrs.getString("MenuName"));
				mdlMenu.setMenuUrl(jrs.getString("MenuUrl"));
				mdlMenu.setType(jrs.getString("Type"));
				mdlMenu.setLevel(jrs.getString("Level"));
				
				listmdlMenu.add(mdlMenu);
				
				String sqlLoadMenuLv1 = "{call wb_LoadMenu_lv1(?)}";
				pstmLoadMenuLv1 = connection.prepareStatement(sqlLoadMenuLv1);
				pstmLoadMenuLv1.setString(1, mdlMenu.getType());
				jrs2 = pstmLoadMenuLv1.executeQuery();
				
				
				while(jrs2.next()) {
					model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					mdlMenuLv1.setMenuID(jrs2.getString("MenuID"));
					mdlMenuLv1.setMenuName("- "+jrs2.getString("MenuName"));
					mdlMenuLv1.setMenuUrl(jrs2.getString("MenuUrl"));
					mdlMenuLv1.setType(jrs2.getString("Type"));
					mdlMenuLv1.setLevel(jrs2.getString("Level"));
					
					listmdlMenu.add(mdlMenuLv1);
					
					String sqlLoadMenuLv2 = "{call wb_LoadMenu_lv2(?,?)}";
					pstmLoadMenuLv2 = connection.prepareStatement(sqlLoadMenuLv2);
					pstmLoadMenuLv2.setString(1, mdlMenu.getType());
					pstmLoadMenuLv2.setString(2, "%" + mdlMenuLv1.getMenuID() + "%");
					jrs3 = pstmLoadMenuLv2.executeQuery();
					
					
					while(jrs3.next()) {
						model.mdlMenu mdlMenuLv2 = new model.mdlMenu();
						mdlMenuLv2.setMenuID(jrs3.getString("MenuID"));
						mdlMenuLv2.setMenuName("- "+jrs3.getString("MenuName"));
						mdlMenuLv2.setMenuUrl(jrs3.getString("MenuUrl"));
						mdlMenuLv2.setType(jrs3.getString("Type"));
						mdlMenuLv2.setLevel(jrs3.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv2);
					}
				}
			}	

		command = pstmLoadMenu.toString() + pstmLoadMenuLv1.toString() + pstmLoadMenuLv2.toString();
			
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMenu", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (pstmLoadMenuLv1 != null) {
					pstmLoadMenuLv1.close();
				}
				if (pstmLoadMenuLv2 != null) {
					pstmLoadMenuLv2.close();
				}
				if (connection != null) {
					connection.close();
				}
				
				if (jrs != null) {
					jrs.close();
				}
				if (jrs2 != null) {
					jrs2.close();
				}
				if (jrs3 != null) {
					jrs3.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadMenu", "close opened connection protocol", user);
			}
		}
		return listmdlMenu;
	}
	
	public static List<model.mdlMenu> LoadMenuByID(String lMenuID,String lCommand, String user) {
		Connection connection = null;
		String command = "";
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();

		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		PreparedStatement pstmLoadMenuLv0 = null;
		ResultSet jrsMenuLv0 = null;
		
		PreparedStatement pstmLoadMenuLv1 = null;
		ResultSet jrsMenuLv1 = null;
		
		PreparedStatement pstmLoadMenuLv2 = null;
		ResultSet jrsMenuLv2 = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			String sqlLoadMenu = "{call wb_LoadMenuById(?)}";
			pstmLoadMenu = connection.prepareStatement(sqlLoadMenu);
			pstmLoadMenu.setString(1, lMenuID);
			command = pstmLoadMenu.toString();
			jrs = pstmLoadMenu.executeQuery();
									
			while(jrs.next()){
				
				String menuLevel = jrs.getString("Level");
				if(menuLevel.contentEquals("0"))
				{
					String sqlLoadMenuLv0 = "{call wb_LoadMenuById_lv0(?)}";
					pstmLoadMenuLv0 = connection.prepareStatement(sqlLoadMenuLv0);
					pstmLoadMenuLv0.setString(1, jrs.getString("Type"));
					command += pstmLoadMenuLv0.toString();
					jrsMenuLv0 = pstmLoadMenuLv0.executeQuery();
					
					while(jrsMenuLv0.next())
					{
						model.mdlMenu mdlMenuLv0 = new model.mdlMenu();
					
						mdlMenuLv0.setMenuID(jrsMenuLv0.getString("MenuID"));
						
						if(jrsMenuLv0.getString("Level").contentEquals("0"))
							mdlMenuLv0.setMenuName(jrsMenuLv0.getString("MenuName"));
						else
							mdlMenuLv0.setMenuName("- " + jrsMenuLv0.getString("MenuName"));
							
						mdlMenuLv0.setMenuUrl(jrsMenuLv0.getString("MenuUrl"));
						mdlMenuLv0.setType(jrsMenuLv0.getString("Type"));
						mdlMenuLv0.setLevel(jrsMenuLv0.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv0);
					}
				}
				else if(menuLevel.contentEquals("1"))
				{
					String sqlLoadMenuLv1 = "";
					if(lCommand.contentEquals("Add"))
					{
						sqlLoadMenuLv1 = "{call wb_LoadMenuById_lv1(?,?)}";
						
						pstmLoadMenuLv1 = connection.prepareStatement(sqlLoadMenuLv1);
						pstmLoadMenuLv1.setString(1, "%" + jrs.getString("MenuID") + "%");
						pstmLoadMenuLv1.setString(2, jrs.getString("Type"));
						pstmLoadMenuLv1.addBatch();
					}
					else
					{
						sqlLoadMenuLv1 = "{call wb_LoadMenuById_lv1_1(?)}";
						
						pstmLoadMenuLv1 = connection.prepareStatement(sqlLoadMenuLv1);
						pstmLoadMenuLv1.setString(1, "%" + jrs.getString("MenuID") + "%");
						pstmLoadMenuLv1.addBatch();
					}
					command += pstmLoadMenuLv1.toString();
					jrsMenuLv1 = pstmLoadMenuLv1.executeQuery();

					
					while(jrsMenuLv1.next())
					{
						model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					
						mdlMenuLv1.setMenuID(jrsMenuLv1.getString("MenuID"));
						
						if(jrsMenuLv1.getString("Level").contentEquals("0"))
							mdlMenuLv1.setMenuName(jrsMenuLv1.getString("MenuName"));
						else
							mdlMenuLv1.setMenuName("- " + jrsMenuLv1.getString("MenuName"));
							
						mdlMenuLv1.setMenuUrl(jrsMenuLv1.getString("MenuUrl"));
						mdlMenuLv1.setType(jrsMenuLv1.getString("Type"));
						mdlMenuLv1.setLevel(jrsMenuLv1.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv1);
					}
				}
				else
				{
					String sqlLoadMenuLv2 = "";
					if(lCommand.contentEquals("Add"))
					{
						sqlLoadMenuLv2 = "{call wb_LoadMenuById_lv2(?,?,?)}";
						
						pstmLoadMenuLv2 = connection.prepareStatement(sqlLoadMenuLv2);
						pstmLoadMenuLv2.setString(1, jrs.getString("MenuID"));
						pstmLoadMenuLv2.setString(2, jrs.getString("Type"));
						pstmLoadMenuLv2.setString(3, jrs.getString("MenuID").substring(0, 4));
					}
					else
					{
						sqlLoadMenuLv2 = "{call wb_LoadMenuById_lv2_1(?)}";
						
						pstmLoadMenuLv2 = connection.prepareStatement(sqlLoadMenuLv2);
						pstmLoadMenuLv2.setString(1, jrs.getString("MenuID"));
					}
					command += pstmLoadMenuLv2.toString();
					jrsMenuLv2 = pstmLoadMenuLv2.executeQuery();
					
					while(jrsMenuLv2.next())
					{
						model.mdlMenu mdlMenuLv2 = new model.mdlMenu();
					
						mdlMenuLv2.setMenuID(jrsMenuLv2.getString("MenuID"));
						
						if(jrsMenuLv2.getString("Level").contentEquals("0"))
							mdlMenuLv2.setMenuName(jrsMenuLv2.getString("MenuName"));
						else
							mdlMenuLv2.setMenuName("- " + jrsMenuLv2.getString("MenuName"));
							
						mdlMenuLv2.setMenuUrl(	jrsMenuLv2.getString("MenuUrl"));
						mdlMenuLv2.setType(jrsMenuLv2.getString("Type"));
						mdlMenuLv2.setLevel(jrsMenuLv2.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv2);
					}
				}
			}			
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMenuByID", command, user);
		}
		finally{
			//close the opened connection
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (pstmLoadMenuLv0 != null) {
					pstmLoadMenuLv0.close();
				}
				if (pstmLoadMenuLv1 != null) {
					pstmLoadMenuLv1.close();
				}
				if (pstmLoadMenuLv2 != null) {
					pstmLoadMenuLv2.close();
				}
				
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
				if (jrsMenuLv0 != null) {
					jrsMenuLv0.close();
				}
				if (jrsMenuLv1 != null) {
					jrsMenuLv1.close();
				}
				if (jrsMenuLv2 != null) {
					jrsMenuLv2.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadMenuByID", "close opened connection protocol", user);
			}
		}
		return listmdlMenu;
	}
	
	public static String InsertUserRule( String lRoleID,String lRoleName, String[] llistAllMenuID, String[] llistAllowedMenuID, String[] llistEditableMenuID, String user) 
	{	
		String command = "";
		String result = "";
		
		Connection connection = null;
		PreparedStatement pstmInsertRole = null;
		PreparedStatement pstmInsertAccessRole = null;
		PreparedStatement pstmUpdateIsAccess = null;
		PreparedStatement pstmUpdateIsModify = null;
		PreparedStatement pstmCheck = null;
		ResultSet jrsCheck = null;
		Boolean check = false;
			
		try{
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//check duplicate role id
			String sqlCheck = "{call wb_LoadRoleById(?)}";
			pstmCheck = connection.prepareStatement(sqlCheck);
			
			//insert sql parameter and execute for check
			pstmCheck.setString(1,  lRoleID);
			jrsCheck = pstmCheck.executeQuery();
			command = pstmCheck.toString();
			
			while(jrsCheck.next()){
				check = true;
			}
				//if no duplicate
				if(check==false)
				{
					connection.setAutoCommit(false);
					//Execute insert Role Before
					pstmInsertRole = connection.prepareStatement("{call wb_InsertRole(?,?)}");
					pstmInsertRole.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
					pstmInsertRole.setString(2,  ValidateNull.NulltoStringEmpty(lRoleName));
					command = pstmInsertRole.toString();
					pstmInsertRole.executeUpdate();

					pstmUpdateIsAccess = connection.prepareStatement("{call wb_UpdateIsAccess(?,?,?,?)}");
					pstmUpdateIsModify = connection.prepareStatement("{call wb_UpdateIsModify(?,?,?)}");
						
					//insert access role process parameter
					pstmInsertAccessRole = connection.prepareStatement("{call wb_InsertAccessRole(?,?,?,?,?)}");
					for(String lmenuid : llistAllMenuID)
					{
						pstmInsertAccessRole.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
						pstmInsertAccessRole.setString(2,  ValidateNull.NulltoStringEmpty(lmenuid));
						pstmInsertAccessRole.setBoolean(3, false);
						pstmInsertAccessRole.setBoolean(4, false);
						pstmInsertAccessRole.setString(5,  ValidateNull.NulltoStringEmpty(lRoleID));
							
						command += pstmInsertAccessRole.toString();
						pstmInsertAccessRole.executeUpdate();
					}
					
					//update isaccess process parameter

					for(String lmenuid : llistAllowedMenuID)
					{
						//nanda						
						pstmUpdateIsAccess.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
						pstmUpdateIsAccess.setString(2,  ValidateNull.NulltoStringEmpty(lmenuid));
						pstmUpdateIsAccess.setBoolean(3, true);
						pstmUpdateIsAccess.setBoolean(4, false);
								
						command += pstmUpdateIsAccess.toString();
						pstmUpdateIsAccess.executeUpdate();
					}
					
						
					//update ismodify process parameter
					for(String lmenuid : llistEditableMenuID)
					{
						pstmUpdateIsModify.setBoolean(1, true);
						pstmUpdateIsModify.setString(2, ValidateNull.NulltoStringEmpty(lRoleID));
						pstmUpdateIsModify.setString(3, ValidateNull.NulltoStringEmpty(lmenuid));
							
						command += pstmUpdateIsModify.toString();
						pstmUpdateIsModify.executeUpdate();
					}
					
						
					connection.commit(); //commit transaction if all of the proccess is running well
						
					result = "SuccessInsert";
				}
				//if duplicate
				else {
					result = "The Role Id Already Exist";
				}
		}
		
		catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "TransactionInsertUserRule", command , user);
		  			result = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "InsertUserRule", command , user);
    			result = "Database Error";
			}
			finally {
				try{
					if (pstmInsertRole != null) {
						pstmInsertRole.close();
					}
					if (pstmInsertAccessRole != null) {
						pstmInsertAccessRole.close();
					}
					if (pstmUpdateIsAccess != null) {
						pstmUpdateIsAccess.close();
					}
					if (pstmUpdateIsModify != null) {
						pstmUpdateIsModify.close();
					}
					if (pstmCheck != null) {
						 pstmCheck.close();
					 }
		        
					connection.setAutoCommit(true);
		        
					if (connection != null) {
						connection.close();
					}
					if (jrsCheck != null) {
						 jrsCheck.close();
					 }
				}
				catch(Exception ex){
					LogAdapter.InsertLogExc(ex.toString(), "InsertUserRule", "close opened connection protocol", user);
				}
		    }
			
			return result;
	}
	
	public static String UpdateUserRule( String lRoleID,String lRoleName, String[] llistAllMenuID, String[] llistAllowedMenuID, String[] llistEditableMenuID, String user) {	
	String command = "";
	String result = "";
	
	Connection connection = null;
	PreparedStatement pstmUpdateIsAccess = null;
	PreparedStatement pstmUpdateIsModify = null;
	PreparedStatement pstmUpdateIsAccessAll = null;
	PreparedStatement pstmUpdateIsModifyAll = null;
	try{ 
		connection = database.RowSetAdapter.getConnection();
		connection.setAutoCommit(false);
		
		pstmUpdateIsModifyAll = connection.prepareStatement("{call wb_UpdateIsModifyAll(?,?,?)}");	
		pstmUpdateIsModifyAll.setBoolean(1, false);
		pstmUpdateIsModifyAll.setString(2, ValidateNull.NulltoStringEmpty(lRoleID));
		pstmUpdateIsModifyAll.setString(3, ValidateNull.NulltoStringEmpty(lRoleName));
		
		command += pstmUpdateIsModifyAll.toString();
		pstmUpdateIsModifyAll.executeUpdate();
		
		pstmUpdateIsAccessAll = connection.prepareStatement("{call wb_UpdateIsAccessAll(?,?)}");
		pstmUpdateIsAccessAll.setBoolean(1, false);
		pstmUpdateIsAccessAll.setString(2, ValidateNull.NulltoStringEmpty(lRoleID));
		
		command += pstmUpdateIsAccessAll.toString();
		pstmUpdateIsAccessAll.executeUpdate();
		
		connection.commit();
		result = "SuccessUpdate";
	}
	catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	           	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateUserRule", command , user);
	           	result = "Database Error";
	            }
	        }
	        
	        LogAdapter.InsertLogExc(e.toString(), "UpdateUserRule", command , user);
	    	 result = "Database Error";
	}
	finally{
		//close the opened connection
		try{
		if (pstmUpdateIsAccessAll != null) {
		pstmUpdateIsAccessAll.close();
		}
		if (pstmUpdateIsModifyAll != null) {
		pstmUpdateIsModifyAll.close();
		}
		connection.setAutoCommit(true);
		if (connection != null) {
		connection.close();
		}
		}
		catch(Exception ex){
		LogAdapter.InsertLogExc(ex.toString(), "UpdateUser", "close opened connection protocol", user);
		}
	}
	
	try{
		connection = database.RowSetAdapter.getConnection();
		connection.setAutoCommit(false);
		
		pstmUpdateIsAccess = connection.prepareStatement("{call wb_UpdateIsAccess(?,?,?,?)}");
		//pstmUpdateIsAccess = connection.prepareStatement("{call wb_InsertAccessRole(?,?,?,?,?)}");
		pstmUpdateIsModify = connection.prepareStatement("{call wb_UpdateIsModify(?,?,?)}");
		
		
		//update isaccess process parameter	
		if (llistAllowedMenuID.length != 0)
		{
		
		//insert access role process parameter
		for(String lmenuid : llistAllowedMenuID)
		{	
		pstmUpdateIsAccess.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
		pstmUpdateIsAccess.setString(2,  ValidateNull.NulltoStringEmpty(lmenuid));
		pstmUpdateIsAccess.setBoolean(3, true);
		pstmUpdateIsAccess.setBoolean(4, false);
		
		//	 pstmUpdateIsAccess.setBoolean(1, true);
		//	 pstmUpdateIsAccess.setString(2, ValidateNull.NulltoStringEmpty(lRoleID));
		//	 pstmUpdateIsAccess.setString(3, ValidateNull.NulltoStringEmpty(lmenuid));
		
		command += pstmUpdateIsAccess.toString();
		pstmUpdateIsAccess.executeUpdate();
		}
		}
		
		//update ismodify process parameter	
		if (llistEditableMenuID.length != 0){
		 for(String lmenuid : llistEditableMenuID)
		{
		 
		pstmUpdateIsModify.setBoolean(1, true);
		pstmUpdateIsModify.setString(2, ValidateNull.NulltoStringEmpty(lRoleID));
		pstmUpdateIsModify.setString(3, ValidateNull.NulltoStringEmpty(lmenuid));
		
		command += pstmUpdateIsModify.toString();
		pstmUpdateIsModify.executeUpdate();
		}
	}
	
	connection.commit(); //commit transaction if all of the proccess is running well
	
	result = "SuccessUpdate";
	}
	
	
	catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	           	LogAdapter.InsertLogExc(excep.toString(), "TransactionUpdateUserRule", command , user);
	           	result = "Database Error";
	            }
	        }
	        
	        LogAdapter.InsertLogExc(e.toString(), "UpdateUserRule", command , user);
	    	 result = "Database Error";
	}
	finally {
		try{
		if (pstmUpdateIsAccess != null) {
		pstmUpdateIsAccess.close();
		}
		if (pstmUpdateIsModify != null) {
		pstmUpdateIsModify.close();
		}
		if (pstmUpdateIsAccessAll != null) {
		pstmUpdateIsAccessAll.close();
		}
		if (pstmUpdateIsModifyAll != null) {
		pstmUpdateIsModifyAll.close();
		}
		        
		connection.setAutoCommit(true);
		        
		if (connection != null) {
		connection.close();
		}
	}
	catch(Exception ex){
		LogAdapter.InsertLogExc(ex.toString(), "UpdateUserRule", "close opened connection protocol", user);
	}
	    }
	
	return result;
}
	
	public static String DeleteUserRule(String lRoleID, String user) 
	{	
		String command = "";
		String result = "";
		
		Connection connection = null;
		PreparedStatement pstmRole = null;
		PreparedStatement pstmAccessRole = null;
			
		try{
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			
			//Execute insert Role Before
			pstmRole = connection.prepareStatement("{call wb_DeleteRole(?)}");
			pstmRole.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
			command = pstmRole.toString();
			pstmRole.executeUpdate();
			
			pstmAccessRole = connection.prepareStatement("{call wb_DeleteAccessRole(?)}");
			pstmAccessRole.setString(1,  ValidateNull.NulltoStringEmpty(lRoleID));
			command = pstmAccessRole.toString();
			pstmAccessRole.executeUpdate();
				
			connection.commit(); //commit transaction if all of the proccess is running well
				
			result = "SuccessDelete";
		}
		
			
		catch (Exception e) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
		           	LogAdapter.InsertLogExc(excep.toString(), "TransactionDeleteUserRule", command , user);
		  			result = "Database Error";
		            }
		        }
		        
		        LogAdapter.InsertLogExc(e.toString(), "DeleteUserRule", command , user);
    			result = "Database Error";
			}
			finally {
				try{
					if (pstmRole != null) {
						pstmRole.close();
					}
					if (pstmAccessRole != null) {
						pstmAccessRole.close();
					}
		        
					connection.setAutoCommit(true);
		        
					if (connection != null) {
						connection.close();
					}
				}
				catch(Exception ex){
					LogAdapter.InsertLogExc(ex.toString(), "DeleteUserRule", "close opened connection protocol", user);
				}
		    }
			
			return result;
	}

	public static List<model.mdlMenu> LoadAllowedMenu(String lRoleID, String user) {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		String command = "";
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		PreparedStatement pstmLoadMenuLv1 = null;
		ResultSet jrs2 = null;
		
		PreparedStatement pstmLoadMenuLv2 = null;
		ResultSet jrs3 = null;
		try{
			connection = database.RowSetAdapter.getConnection();;
			
			pstmLoadMenu = connection.prepareStatement("{call wb_LoadAllowedMenu(?)}");
			pstmLoadMenu.setString(1, lRoleID);
			pstmLoadMenu.addBatch();
			jrs = pstmLoadMenu.executeQuery();
			command = pstmLoadMenu.toString();
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				mdlMenu.setMenuName(jrs.getString("MenuName"));
				mdlMenu.setMenuUrl(jrs.getString("MenuUrl"));
				mdlMenu.setType(jrs.getString("Type"));
				mdlMenu.setLevel(jrs.getString("Level"));
				
				listmdlMenu.add(mdlMenu);
				
				
				pstmLoadMenuLv1 = connection.prepareStatement("{call wb_LoadAllowedMenu_lv1(?,?)}");
				pstmLoadMenuLv1.setString(1, lRoleID);
				pstmLoadMenuLv1.setString(2, mdlMenu.getType());
				pstmLoadMenuLv1.addBatch();
				jrs2 = pstmLoadMenuLv1.executeQuery();
				command += pstmLoadMenuLv1.toString();
				
				while(jrs2.next()) {
					model.mdlMenu mdlMenuLv1 = new model.mdlMenu();
					mdlMenuLv1.setMenuID(jrs2.getString("MenuID"));
					mdlMenuLv1.setMenuName("- "+jrs2.getString("MenuName"));
					mdlMenuLv1.setMenuUrl(jrs2.getString("MenuUrl"));
					mdlMenuLv1.setType(jrs2.getString("Type"));
					mdlMenuLv1.setLevel(jrs2.getString("Level"));
					
					listmdlMenu.add(mdlMenuLv1);
					
					
					pstmLoadMenuLv2 = connection.prepareStatement("{call wb_LoadAllowedMenu_lv2(?,?,?)}");
					pstmLoadMenuLv2.setString(1, lRoleID);
					pstmLoadMenuLv2.setString(2, mdlMenu.getType());
					pstmLoadMenuLv2.setString(3, "%" + mdlMenuLv1.getMenuID() + "%");
					pstmLoadMenuLv2.addBatch();
					jrs3 = pstmLoadMenuLv2.executeQuery();
					command += pstmLoadMenuLv2.toString();
					
					while(jrs3.next()) {
						model.mdlMenu mdlMenuLv2 = new model.mdlMenu();
						mdlMenuLv2.setMenuID(jrs3.getString("MenuID"));
						mdlMenuLv2.setMenuName("- "+jrs3.getString("MenuName"));
						mdlMenuLv2.setMenuUrl(jrs3.getString("MenuUrl"));
						mdlMenuLv2.setType(jrs3.getString("Type"));
						mdlMenuLv2.setLevel(jrs3.getString("Level"));
						
						listmdlMenu.add(mdlMenuLv2);
					}
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadAllowedMenu", command , user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (pstmLoadMenuLv1 != null) {
					pstmLoadMenuLv1.close();
				}
				if (pstmLoadMenuLv2 != null) {
					pstmLoadMenuLv2.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
				if (jrs2 != null) {
					jrs2.close();
				}
				if (jrs3 != null) {
					jrs3.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadAllowedMenu", "close opened connection protocol", user);
			}
		}

		return listmdlMenu;
	}

	public static List<model.mdlMenu> LoadCRUDMenu(String user)  {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		String command = "";
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmLoadMenu = connection.prepareStatement("{call wb_LoadCRUDMenu()}");
			jrs = pstmLoadMenu.executeQuery();
			command = pstmLoadMenu.toString();
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				mdlMenu.setMenuName(jrs.getString("MenuName"));
				mdlMenu.setMenuUrl(jrs.getString("MenuUrl"));
				mdlMenu.setType(jrs.getString("Type"));
				mdlMenu.setLevel(jrs.getString("Level"));
				mdlMenu.setCRUD(jrs.getBoolean("CRUD"));
				mdlMenu.setIsModify(false);
				
				listmdlMenu.add(mdlMenu);
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadCRUDMenu", command , user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadCRUDMenu", "close opened connection protocol", user);
			}
		}

		return listmdlMenu;
	}
	
	public static List<model.mdlMenu> LoadEditableMenu(String user) {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		String command = "";
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmLoadMenu = connection.prepareStatement("{call wb_LoadEditableMenu()}");
			jrs = pstmLoadMenu.executeQuery();
			command = pstmLoadMenu.toString();
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				mdlMenu.setMenuName(jrs.getString("MenuName"));
				mdlMenu.setIsModify(jrs.getBoolean("IsModify"));
				
				listmdlMenu.add(mdlMenu);
			}
			if(!jrs.next())
			{
				listmdlMenu = LoadCRUDMenu(user);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenu", command , user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenu", "close opened connection protocol", user);
			}
		}

		return listmdlMenu;
	}

	public static List<model.mdlMenu> LoadEditableMenu(String lRoleID, String user) {
		List<model.mdlMenu> listmdlMenu = new ArrayList<model.mdlMenu>();
		
		String command = "";
		Connection connection = null;
		PreparedStatement pstmLoadMenu = null;
		ResultSet jrs = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmLoadMenu = connection.prepareStatement("{call wb_LoadEditableMenubyRoleID(?)}");
			pstmLoadMenu.setString(1, lRoleID);
																																										pstmLoadMenu.addBatch();
			jrs = pstmLoadMenu.executeQuery();
			command = pstmLoadMenu.toString();
									
			while(jrs.next()){
				model.mdlMenu mdlMenu = new model.mdlMenu();
				mdlMenu.setMenuID(jrs.getString("MenuID"));
				mdlMenu.setMenuName(jrs.getString("MenuName"));
				mdlMenu.setIsModify(jrs.getBoolean("IsModify"));
				
				listmdlMenu.add(mdlMenu);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenu", command , user);
		}
		finally {
			try{
				if (pstmLoadMenu != null) {
					pstmLoadMenu.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadEditableMenu", "close opened connection protocol", user);
			}
		}

		return listmdlMenu;
	}

	public static Boolean CheckMenu(String MenuURL, String UserID)  {
		Connection connection = null;
		PreparedStatement pstmCheckMenu = null;
		ResultSet jrs = null;
		Boolean CheckMenu = false;
		String command = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstmCheckMenu = connection.prepareStatement("{call wb_LoadMenu_IsAccess_Status(?,?)}");
			pstmCheckMenu.setString(1, MenuURL);
			pstmCheckMenu.setString(2, UserID);
			pstmCheckMenu.addBatch();
			jrs = pstmCheckMenu.executeQuery();
			command = pstmCheckMenu.toString();
									
			while(jrs.next()){
				if(jrs.getBoolean("IsAccess")==true)
				{
					CheckMenu = true;
				}
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "CheckMenu", command , UserID);
		}
		finally {
			try{
				if (pstmCheckMenu != null) {
					pstmCheckMenu.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "CheckMenu", "close opened connection protocol", UserID);
			}
		}

		return CheckMenu;
	}

	public static String SetMenuButtonStatus(String MenuURL, String UserID)  {
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String ButtonStatus = "enabled";
		String command = "";
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadMenu_IsModify_Status(?,?)}");
			pstm.setString(1, MenuURL);
			pstm.setString(2, UserID);
			pstm.addBatch();
			jrs = pstm.executeQuery();
			command = pstm.toString();
									
			while(jrs.next()){
				if(jrs.getBoolean("IsModify")==false)
				{
					ButtonStatus = "disabled";
				}
			}	
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "SetMenuButtonStatus", command , UserID);
		}
		finally {
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "SetMenuButtonStatus", "close opened connection protocol", UserID);
			}
		}

		return ButtonStatus;
	}
	
}
