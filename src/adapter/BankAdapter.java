package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

public class BankAdapter {

	public static List<model.mdlBank> LoadBank(String user) {
		
		//define local variable
		List<model.mdlBank> BankList = new ArrayList<model.mdlBank>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = null;
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadBank()}";
			pstm = connection.prepareStatement(sql);
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlBank Bank = new model.mdlBank();
				
				Bank.setBankID(jrs.getString("BankID"));
				Bank.setBankName(jrs.getString("BankName"));
				
				BankList.add(Bank);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadBank", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadBank", "close opened connection protocol", user);
			}
		}

		return BankList;
	}
	
}
