package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import java.sql.CallableStatement;
//import com.mysql.jdbc.CallableStatement;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlMerchant;
import model.mdlUser;

public class MerchantAdapter {

	public static model.mdlMerchantBalance LoadMerchantBalance(String lParamMerchantID) {
		
		//define local variable
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
		model.mdlMerchantBalance mdlMerchantBalance = new model.mdlMerchantBalance();
		
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadMerchantBalance(?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  lParamMerchantID);
			command = pstm.toString();
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				mdlMerchantBalance.MerchantID = jrs.getString("MerchantID");
				mdlMerchantBalance.MerchantName = jrs.getString("MerchantName");
				mdlMerchantBalance.Balance = jrs.getString("Balance");
				mdlMerchantBalance.MerchantBrandID = jrs.getString("BrandID");
				mdlMerchantBalance.MerchantClientID = jrs.getString("ClientID");
				mdlMerchantBalance.MerchantEncryptionKeyID = jrs.getString("EncryptionKeyID");
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMerchantBalance", command, lParamMerchantID);
		}
		finally {
			try{
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadMerchantBalance", "close opened connection protocol", lParamMerchantID);
			}
		}

		return mdlMerchantBalance;
	}
	
	public static List<model.mdlMerchant> LoadMerchant(mdlUser pUser) {
		
		//define local variable
		List<model.mdlMerchant> MerchantList = new ArrayList<model.mdlMerchant>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadMerchant(?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  pUser.Area);
			pstm.setString(2,  pUser.Brand);
			pstm.addBatch();
			pstm.executeBatch();
			
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlMerchant Merchant = new model.mdlMerchant();
				
				Merchant.setMerchantID(jrs.getString("MerchantID"));
				Merchant.setMerchantName(jrs.getString("MerchantName"));
				Merchant.setMerchantPIC(jrs.getString("PIC"));
				Merchant.setMerchantEmail(jrs.getString("Email"));
				Merchant.setMerchantContact(jrs.getString("Contact"));
				Merchant.setMerchantBrand(jrs.getString("BrandID"));
				Merchant.setMerchantType(jrs.getString("TypeID"));
				Merchant.setMerchantTypeName(jrs.getString("TypeName"));
				Merchant.setMerchantArea(jrs.getString("AreaID"));
				Merchant.setMerchantAreaName(jrs.getString("AreaName"));
				Merchant.setMerchantAddress(jrs.getString("Address"));
				Merchant.setMerchantProvinceID(jrs.getString("ProvinceID"));
				Merchant.setMerchantDistrict(jrs.getString("DistrictID"));
				Merchant.setMerchantDistrictName(jrs.getString("DistrictName"));
				Merchant.setMerchantSubDistrict(jrs.getString("SubDistrictID"));
				Merchant.setMerchantSubDistrictName(jrs.getString("SubDistrictName"));
				Merchant.setMerchantBank(jrs.getString("BankID"));
				Merchant.setMerchantBankName(jrs.getString("BankName"));
				Merchant.setAccountName(jrs.getString("AccountName"));
				Merchant.setMerchantAccountNumber(jrs.getString("AccountNumber"));
				Merchant.setMerchantProvince(jrs.getString("ProvinceName"));
				
				MerchantList.add(Merchant);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadMerchant", command, pUser.UserId);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadMerchant", "close opened connection protocol", pUser.UserId);
			}
		}

		return MerchantList;
	}

	public static List<model.mdlMerchantType> LoadMerchantType(String user) {
		
		//define local variable
		List<model.mdlMerchantType> MerchantTypeList = new ArrayList<model.mdlMerchantType>();
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String command = "";
				
		try {
			//define connection
			connection = database.RowSetAdapter.getConnection();
			
			//call store procedure
			String sql = "{call wb_LoadMerchantType()}";
			pstm = connection.prepareStatement(sql);
					
			//execute store procedure
			jrs = pstm.executeQuery();
			command = pstm.toString();
			
			while(jrs.next()){
				model.mdlMerchantType MerchantType = new model.mdlMerchantType();
				
				MerchantType.setMerchantTypeID(jrs.getString("TypeID"));
				MerchantType.setMerchantTypeName(jrs.getString("TypeName"));
				
				MerchantTypeList.add(MerchantType);
			}
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "LoadMerchantType", command, user);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "LoadMerchantType", "close opened connection protocol", user);
			}
		}

		return MerchantTypeList;
	}

	public static String InsertMerchant(mdlMerchant lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		PreparedStatement pstmInsertUser = null;
		PreparedStatement pstmCheck = null;
		ResultSet jrs = null;
		Boolean check = false;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		
		//check duplicate merchant id
		String sqlCheck = "{call wb_LoadMerchantById(?)}";
		pstmCheck = connection.prepareStatement(sqlCheck);
		
		//insert sql parameter and execute for check
		pstmCheck.setString(1,  lParam.getMerchantID());
		jrs = pstmCheck.executeQuery();
		command = pstmCheck.toString();
		
		while(jrs.next()){
			check = true;
		}
			//if no duplicate
			if(check==false)
			{
				connection.setAutoCommit(false); //for transaction scope need
				
				//call store procedure
				String sql = "{call wb_InsertMerchant(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
				pstm = connection.prepareStatement(sql);
				
				String sqlInsertUser = "{call wb_InsertUser(?,?,?,?,?,?,?,?,?)}";
				pstmInsertUser = connection.prepareStatement(sqlInsertUser);
				
				//insert sql parameter
				pstm.setString(1,  lParam.getMerchantID());
				pstm.setString(2,  lParam.getMerchantName());
				pstm.setString(3, lParam.getMerchantEmail());
				pstm.setString(4, lParam.getMerchantContact());
				pstm.setString(5, lParam.getMerchantPIC());
				pstm.setString(6, lParam.getMerchantBrand());
				pstm.setString(7, lParam.getMerchantType());
				pstm.setString(8, lParam.getMerchantArea());
				pstm.setString(9, lParam.getMerchantAddress());
				pstm.setString(10, lParam.getMerchantProvince());
				pstm.setString(11, lParam.getMerchantDistrict());
				pstm.setString(12, lParam.getMerchantSubDistrict());
				pstm.setString(13, lParam.getMerchantBank());
				pstm.setString(14, lParam.getMerchantAccountNumber());
				pstm.setString(15, user);
				pstm.setString(16, user);
				pstm.setString(17, LocalDateTime.now().toString());
				pstm.setString(18, LocalDateTime.now().toString());
				pstm.setString(19, lParam.getAccountName());
				command = pstm.toString();
				pstm.executeUpdate();
				//pstm.addBatch();
				//pstm.executeBatch();
				
				//insert sqlInserUser parameter
				pstmInsertUser.setString(1,  lParam.getMerchantID());
				pstmInsertUser.setString(2,  lParam.getMerchantName());
				pstmInsertUser.setString(3, Base64Adapter.EncriptBase64(lParam.getMerchantID()));
				pstmInsertUser.setString(4, lParam.getMerchantArea());
				pstmInsertUser.setString(5, "R003");
				pstmInsertUser.setString(6, lParam.getMerchantBrand());
				pstmInsertUser.setString(7, "merchant");
				pstmInsertUser.setString(8, user);
				pstmInsertUser.setString(9, user);
				command = pstmInsertUser.toString();
				pstmInsertUser.executeUpdate();
				
				connection.commit(); //commit transaction if all of the proccess is running well
				
				result = "Success Insert Merchant";
			}
			//if duplicate
			else {
				result = "The Merchant Id Already Exist";
			}
		}
		catch(Exception ex) {
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "RollBackInsertMerchant", command , user);
	    			result = "Database Error";
	            }
	        }
			
			LogAdapter.InsertLogExc(ex.toString(), "InsertMerchant", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (pstmInsertUser != null) {
					 pstmInsertUser.close();
				 }
				 if (pstmCheck != null) {
					 pstmCheck.close();
				 }
				 connection.setAutoCommit(true);
				 if (connection != null) {
					 connection.close();
				 }
				 if (jrs != null) {
					 jrs.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "InsertMerchant", "close opened connection protocol", user);
			}
		}
		
		return result;
	}

	public static String UpdateMerchant(mdlMerchant lParam, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		
		//call store procedure
		String sql = "{call wb_UpdateMerchant(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
		pstm = connection.prepareStatement(sql);
		
		//insert sql parameter
		pstm.setString(1,  lParam.getMerchantName());
		pstm.setString(2,  lParam.getMerchantEmail());
		pstm.setString(3, lParam.getMerchantContact());
		pstm.setString(4, lParam.getMerchantPIC());
		pstm.setString(5, lParam.getMerchantBrand());
		pstm.setString(6, lParam.getMerchantType());
		pstm.setString(7, lParam.getMerchantArea());
		pstm.setString(8, lParam.getMerchantAddress());
		pstm.setString(9, lParam.getMerchantProvince());
		pstm.setString(10, lParam.getMerchantDistrict());
		pstm.setString(11, lParam.getMerchantSubDistrict());
		pstm.setString(12, lParam.getMerchantBank());
		pstm.setString(13, lParam.getMerchantAccountNumber());
		pstm.setString(14, user);
		pstm.setString(15, LocalDateTime.now().toString());
		pstm.setString(16, lParam.getMerchantID());
		pstm.setString(17, lParam.getAccountName());
		command = pstm.toString();
		pstm.addBatch();
		pstm.executeBatch();
		
		result = "Success Update Merchant";
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "UpdateMerchant", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "UpdateMerchant", "close opened connection protocol", user);
			}
		}
		
		return result;
	}

	public static List<model.mdlMerchant> LoadMerchantByID(String lMerchantID,String lCommand) {
		Connection connection = null;
		String command = "";
		List<model.mdlMerchant> listmdlMerchant = new ArrayList<model.mdlMerchant>();

		PreparedStatement pstmLoadMerchant = null;
		ResultSet jrs = null;
		
		try{
			connection = database.RowSetAdapter.getConnection();
			String sqlLoadMerchant = "{call wb_LoadMerchantById(?)}";
			pstmLoadMerchant = connection.prepareStatement(sqlLoadMerchant);
			pstmLoadMerchant.setString(1, lMerchantID);
			command = pstmLoadMerchant.toString();
			jrs = pstmLoadMerchant.executeQuery();
									
			while(jrs.next()){
				model.mdlMerchant mdlMerchant = new model.mdlMerchant();
				mdlMerchant.setMerchantID(jrs.getString("MerchantID"));
				mdlMerchant.setMerchantName(jrs.getString("MerchantName"));
				listmdlMerchant.add(mdlMerchant);
			}			
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadLoadMerchantByID", command, lMerchantID);
		}
		finally{
			//close the opened connection
			try{
				if (pstmLoadMerchant != null) {
					pstmLoadMerchant.close();
				}
				
				if (connection != null) {
					connection.close();
				}
				if (jrs != null) {
					jrs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadLoadMerchantByID", "close opened connection protocol", lMerchantID);
			}
		}
		return listmdlMerchant;
	}
	
	//<<Nanda 
		public static List<model.mdlMerchant> LoadMerchantbyBrandID(String lBrandID, String lProductID, mdlUser pUser) {
			//define connection
			Connection connection = null;
			PreparedStatement pstm = null;
			ResultSet jrs = null;
			String command = "";
			//define local variable
			List<model.mdlMerchant> MerchantList = new ArrayList<model.mdlMerchant>();
			
			try{
			//execute store procedure
			connection =  database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadMerchantbyBrandID(?,?,?)}");
			pstm.setString(1, "'"+lBrandID+"'");
			pstm.setString(2, "'"+lProductID+"'");
			pstm.setString(3, pUser.Area);
//			pstm.setString(4, "'%"+lMerchantName+"%'");
//			pstm.setString(5, "'%"+lMerchantType+"%'");
			command = pstm.toString();
			jrs = pstm.executeQuery();
			
			while(jrs.next()){
				model.mdlMerchant Merchant = new model.mdlMerchant();
				
				Merchant.setMerchantID(jrs.getString("MerchantID"));
				Merchant.setMerchantName(jrs.getString("MerchantName"));
				Merchant.setMerchantPIC(jrs.getString("PIC"));
				Merchant.setMerchantEmail(jrs.getString("Email"));
				Merchant.setMerchantContact(jrs.getString("Contact"));
				Merchant.setMerchantBrand(jrs.getString("BrandID"));
				Merchant.setMerchantType(jrs.getString("TypeID"));
				Merchant.setMerchantTypeName(jrs.getString("TypeName"));
				Merchant.setMerchantArea(jrs.getString("AreaID"));
				Merchant.setMerchantAddress(jrs.getString("Address"));
				Merchant.setMerchantProvinceID(jrs.getString("ProvinceID"));
				Merchant.setMerchantDistrict(jrs.getString("DistrictID"));
				Merchant.setMerchantSubDistrict(jrs.getString("SubDistrictID"));
				Merchant.setMerchantBank(jrs.getString("BankID"));
				Merchant.setMerchantAccountNumber(jrs.getString("AccountNumber"));
				Merchant.setMerchantProvince(jrs.getString("ProvinceName"));
				
				MerchantList.add(Merchant);
			}
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadMerchantbyBrandID", command, pUser.UserId);
			}
			finally{
				//close the opened connection
				try{
					//close the opened connection
					if (pstm != null) {
						pstm.close();
					}
					if (connection != null) {
						connection.close();
					}
					if (jrs != null) {
						jrs.close();
					}
				}
				catch(Exception ex){
					LogAdapter.InsertLogExc(ex.toString(), "LoadMerchantbyBrandID", "close opened connection protocol", pUser.UserId);
				}
			}

			return MerchantList;
		}
		//>>
		
}
