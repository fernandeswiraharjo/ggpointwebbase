package adapter;
import org.eclipse.core.resources.*;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;

import model.Globals;

public class RefreshFolderAdapter {

	public static void RefreshFolder(String user) {
		try {
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
	        IProject project = root.getProject("GGMerchantWeb");
	        IPath path = project.getFullPath().append("WebContent/mainform/image");
	        IFile iFile  = root.getFile(path);

	        iFile.refreshLocal(IResource.DEPTH_INFINITE, null);
		}
		catch(CoreException ex)
		{
			LogAdapter.InsertLogExc(ex.toString(), "RefreshFolder", "", user);
		}
        
   }
	
}
