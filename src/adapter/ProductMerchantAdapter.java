package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import model.Globals;
import model.mdlMerchant;
import model.mdlProductMerchant;
import model.mdlUser;

public class ProductMerchantAdapter {

	public static List<model.mdlProductMerchant> LoadMerchantProduct(mdlUser pUser){	
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		List<model.mdlProductMerchant> mdlProductMerchantList = new ArrayList<model.mdlProductMerchant>();
		try{
			connection = database.RowSetAdapter.getConnection();
			pstm = connection.prepareStatement("{call wb_LoadMerchantProduct(?,?)}");
			pstm.setString(1, pUser.Area);
			pstm.setString(2, pUser.Brand);
			
			command = pstm.toString();
			rs = pstm.executeQuery();	
		
			while(rs.next())
			{
				model.mdlProductMerchant mdlProductMerchant = new model.mdlProductMerchant();				
				mdlProductMerchant.setProductID(rs.getString("ProductID"));
				mdlProductMerchant.setProductName(rs.getString("ProductName"));
				mdlProductMerchant.setMerchantID(rs.getString("MerchantID"));
				mdlProductMerchant.setMerchantName(rs.getString("MerchantName"));
				mdlProductMerchant.setMerchantType(rs.getString("TypeName"));
				mdlProductMerchantList.add(mdlProductMerchant);
			}
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadMerchantProduct", command, pUser.UserId);
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
				}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "LoadMerchantProduct", "close opened connection protocol", pUser.UserId);
			}
		}

		return mdlProductMerchantList;
	}

	public static String InsertMerchantProduct(String lProductID, String[] llistMerchantID, String user)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		String command = "";
		String result = "";
		
		try {
		//define connection
		connection = database.RowSetAdapter.getConnection();
		connection.setAutoCommit(false); //for transaction scope need
		
		for(String lMerchantID : llistMerchantID)
		{
			//call store procedure
			String sql = "{call wb_InsertMerchantProduct(?,?,?,?,?,?)}";
			pstm = connection.prepareStatement(sql);
			
			//insert sql parameter
			pstm.setString(1,  lProductID);
			pstm.setString(2,  lMerchantID);
			pstm.setString(3, user);
			pstm.setString(4, LocalDateTime.now().toString());
			pstm.setString(5, user);
			pstm.setString(6, LocalDateTime.now().toString());
			command = pstm.toString();
			pstm.executeUpdate();
		}
		
		connection.commit(); //commit transaction if all of the proccess is running well
		result = "Success Insert Merchant Product";
		}
		catch(Exception ex) {
			if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "RollBackInsertMerchantProduct", command , user);
	    			result = "Database Error";
	            }
	        }
			
			LogAdapter.InsertLogExc(ex.toString(), "InsertMerchantProduct", command, user);
			result = "Database Error";
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) {
					 pstm.close();
				 }
				 if (connection != null) {
					 connection.close();
				 }
			}
			catch(Exception e) {
				LogAdapter.InsertLogExc(e.toString(), "InsertMerchantProduct", "close opened connection protocol", user);
			}
		}
		
		return result;
	}

	public static String DeleteMerchantProduct(String lMerchantProductlist, String user){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		String command = "";
		String result = "";
		String[] MerchantProductlist = lMerchantProductlist.split(",");
		try{
			connection = database.RowSetAdapter.getConnection();
			
			for(String lMerchantProduct : MerchantProductlist)
			{
				String lProductID = lMerchantProduct.split("&&")[0];
				String lMerchantID = lMerchantProduct.split("&&")[1];
				
				pstm = connection.prepareStatement("{call wb_DeleteMerchantProduct(?,?)}");
				pstm.setString(1, ValidateNull.NulltoStringEmpty(lProductID));
				pstm.setString(2, ValidateNull.NulltoStringEmpty(lMerchantID));
			
				command = pstm.toString();
				rs = pstm.executeQuery();
			}
			
			result = "Success Delete Merchant Product";
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "DeleteMerchantProduct", command, user);
			result = "Delete Merchant Product Not Complete";
		}
		finally{
			//close the opened connection
			try{
				if (pstm != null) {
					pstm.close();
				}
				if (connection != null) {
					connection.close();
				}
				if (rs != null) {
					rs.close();
				}
			}
			catch(Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "DeleteMerchantProduct", "close opened connection protocol", user);
			}
		}
		
		return result;	
	}

	public static String CheckProductMerchantbyBrand(String lProductID, String lBrandID, String user){	
			Connection connection = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;
			String command = "";
			String lResult = "";
			try{
				connection = database.RowSetAdapter.getConnection();
				pstm = connection.prepareStatement("{call wb_CheckProductMerchantbyBrand(?,?)}");
				pstm.setString(1, ValidateNull.NulltoStringEmpty(lBrandID));
				pstm.setString(2, ValidateNull.NulltoStringEmpty(lProductID));
				command = pstm.toString();
				rs = pstm.executeQuery();
			
				while(rs.next())
				{
					lResult = rs.getString("MerchantID");
				}
			}
			catch (Exception ex){
				LogAdapter.InsertLogExc(ex.toString(), "CheckProductMerchantbyBrand", command, user);
			}
			finally{
				//close the opened connection
				try{
					if (pstm != null) {
						pstm.close();
					}
					if (connection != null) {
						connection.close();
					}
					if (rs != null) {
						rs.close();
					}
					}
				catch(Exception ex){
					LogAdapter.InsertLogExc(ex.toString(), "CheckProductMerchantbyBrand", "close opened connection protocol", user);
				}
			}
			return lResult;
		}
		
}
