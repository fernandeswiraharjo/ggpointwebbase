<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | REPORT MERCHANT</title>

<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="mainform/plugins/datepicker/datepicker3.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="formRptMerchant" name="formRptMerchant" action = "${pageContext.request.contextPath}/report_merchant" method="post" target="_blank">

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		<label style="color:#f6f6f6">REPORT MERCHANT</label>
	</h1>
	</section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				
<!-- 				<div id="dvErrorAlert" class="alert alert-danger alert-dismissible" style="display:none;"> -->
<!-- 	          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!-- 	          				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<!-- 	          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>. -->
<!-- 	     					</div> -->
	     					
<%-- 								<c:if test="${condition == 'FailedExport'}"> --%>
<!-- 				 						<div id="dvErrorAlert" class="alert alert-danger alert-dismissible"> -->
<!-- 				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!-- 				          				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%-- 				          				Export Transaction Detail Report Failed. <c:out value="${errorDescription}"/>. --%>
<!-- 				        				</div> -->
<%-- 									</c:if> --%>
					
		<%-- <input id="selectedarea" type="hidden" value="<c:out value="${selectedarea}"/>"> --%>
		<div id="dvShowRpt" class="row">
		<div class="col-md-3">
		<label for="message-text" class="control-label">AREA</label>
		<select id="slArea" name="slArea" class="form-control select2" style="width:100%;">
				<c:forEach items="${listarea}" var="area">
					<option value="<c:out value="${area.areaID}"/>"><c:out value="${area.areaID}" /> - <c:out value="${area.areaName}" /></option>
				</c:forEach>
       		</select>
       	</div>
		<!-- /.col-md-3 -->
		<div class="col-md-3">
			<button id="btnShowRptPdf" name="btnShowRptPdf" type="button" class="btn btn-gg-small2" onclick="FuncButtonShowRpt('pdf')" style="margin-top: 25px">EXPORT PDF</button>
			<button id="btnShowRptExc" name="btnShowRptExc" type="button" class="btn btn-gg-small2" onclick="FuncButtonShowRpt('excel')" style="margin-top: 25px">EXPORT EXCEL</button>
			<input type="hidden" class="form-control" id="txtExportType" name="txtExportType">
		</div>
		<!-- /.col-md-3 -->
		</div>
		<!-- /.row -->
		<br>
				
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
		
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>
<!-- Select2 -->
<script src="mainform/plugins/select2/select2.full.min.js"></script>
<!-- bootstrap datepicker -->
<script src="mainform/plugins/datepicker/bootstrap-datepicker.js"></script>

<script>
	$(function () {
		$(".select2").select2();
		$('#M009').addClass('active');
		$('#M015').addClass('active');
	});
	
	function FuncButtonShowRpt(lParambtn) {
	 	$("#txtExportType").val(lParambtn);
		formRptMerchant.submit();
	}
</script>

</body>
</html>