<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | Mapping Merchant Product</title>
<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
   <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/all.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="formMerchantProduct" name="formMerchantProduct" action = "${pageContext.request.contextPath}/product_merchant" method="post">

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		<label style="color:#f6f6f6">MERCHANT PRODUCT</label>
	</h1>
	</section>
	
	<!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				<c:if test="${condition == 'SuccessInsertMerchantProduct'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Insert Merchant Product Success.
        				</div>
 					</c:if>
<%--  					<c:if test="${condition == 'FailedInsert'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Insert Merchant Product Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
					<c:if test="${condition == 'SuccessDelete'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Delete Merchant Product Success.
        				</div>
 					</c:if>
 					<c:if test="${condition == 'FailedDelete'}">
 						<div class="alert alert-danger alert-dismissible">
          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
          				Delete Merchant Product Failed. <c:out value="${errorDescription}"/>.
        				</div>
					</c:if>
					
					<!-- modal update insert merchant -->
					<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							
							<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
	     					</div>
							
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalUpdateInsert" name="lblTitleModalUpdateInsert"></label></h4>
								</div>
   								<div class="modal-body">
   								
   								<div id="dvProductID" class="form-group">
	         							<label for="recipient-name" class="control-label">PRODUCT ID</label><label id="mrkProductID" for="recipient-name" class="control-label"><small>*</small></label>
         								<small><label id="lblProductName" name="lblProductName" class="control-label"></label></small>
         								<input type="text" class="form-control" id="txtProductID" name="txtProductID" data-toggle="modal" data-target="#ModalGetProduct">
       								</div>
       								<div id="dvBrand" class="form-group">
         								<label for="message-text" class="control-label">BRAND</label><label id="mrkBrand" for="recipient-name" class="control-label"><small>*</small></label>
         								<select id="slBrand" name="slBrand" class="form-control" onfocus="FuncValShowBrand()" onchange="FuncShowMerchant()">
							        	</select>
       								</div>
       								<!-- filter section -->
       								<div id="dvFilter" class="form-group">
	       								<label for="message-text" class="control-label">FILTER</label>
	       								<div class="row">
											<div class="col-md-6">
												<input type="text" class="form-control" id="txtFilterMerchant" name="txtFilterMerchant" placeholder="Merchant Name/Merchant Type...">
											</div>
											<!-- /.dvtxtMerchantFilter -->
											<!-- <div class="col-md-4"> -->
											<!-- <input type="text" class="form-control" id="txtFilterMerchantType" name="txtFilterMerchantType" placeholder="Merchant Type..."> -->
											<!-- </div> -->
											<!-- /.dvtxtMerchantTypeFilter -->
											<!-- <div class="col-md-2"> -->
											<!-- <button type="button" class="btn btn-block btn-default" -->
											<!-- onclick="FuncFilter()">Filter</button> -->
											<!-- </div> -->
											<!-- /.dvbtnFilter -->
										</div>
									</div>
									<!-- /.filter section -->
       								<div id="dvMerchant" class="form-group">
										<label for="message-text" class="control-label">MERCHANT</label><label
											id="mrkMerchant" for="recipient-name" class="control-label">
											<small>*</small>
										</label>
										<div class="panel panel-primary" id="MerchantPanel"
											style="overflow: auto; height: 200px; width: 250px;">
											<div class="panel-body">
											
										<!-- mysql data load merchant by brand will be load here -->
           										<div id="dynamic-content">
           										</div>

											</div>
										</div>
									</div>
									<!-- /.dvMerchant -->
									
									<div class="row">
									<div class="col-md-3">
											<button type="button" class="btn btn-block btn-default"
												onclick="FuncSelectAll()">Select All</button>
										</div>
										<!-- /.dvbtnselectall -->
										<div class="col-md-3">
											<button type="button" class="btn btn-block btn-default"
												onclick="FuncUnselectAll()">Unselect
												All</button>
										</div>
										<!-- /.dvbuttonunselectall -->
									</div>
									<!-- /.row -->
       								
   								</div>
   								<div class="modal-footer">
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
    									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  								</div>
							</div>
						</div>
					</div>
					<!-- /.end modal update & Insert -->

					<!--modal show product data -->
					<div class="modal fade" id="ModalGetProduct" tabindex="-1" role="dialog" aria-labelledby="ModalLabelProduct">
							<div class="modal-dialog" role="document">
										<div class="modal-content">
  											<div class="modal-header">
    											<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    											<h4 class="modal-title" id="ModalLabelProduct"></h4>	
    												
    											
  											</div>
   								<div class="modal-body">
     								
       								<table id="tb_product" class="table table-bordered table-hover">
					        <thead style="background-color: #d2d6de;">
					                <tr>
					                <th>Product ID</th>
									<th>Product Name</th>
									<th>Price</th>
									<th style="width: 20px"></th>
					                </tr>
					        </thead>
					        
					        <tbody>
					        
					        <c:forEach items="${listproduct}" var ="product">
							        <tr>
							        <td><c:out value="${product.productID}" /></td>
									<td><c:out value="${product.productName}" /></td>
									<td><c:out value="${product.price}" /></td>
							        <td><button <c:out value="${buttonstatus}"/> 
							        			type="button" class="btn btn-primary"
							        			data-toggle="modal"
							        			onclick="FuncPassStringProduct('<c:out value="${product.productID}"/>','<c:out value="${product.productName}"/>')"
							        			data-dismiss="modal"
							        	><i class="fa fa-fw fa-check"></i></button>
							        </td>
					        		</tr>
					        		
					        </c:forEach>
					        
					        </tbody>
					        </table>
     								
  								</div>
  								
  								<div class="modal-footer">
    								
  								</div>
										</div>
								</div>
					</div>
					<!-- /. end of modal show product data -->
					
					<!--modal Delete -->
				<div class="example-modal">
				<div class="modal modal-danger" id="ModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
						<div class="modal-content">
									<div class="modal-header">
 										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
   										<span aria-hidden="true">&times;</span></button>
 										<h4 class="modal-title">Alert Delete Merchant Product</h4>
									</div>
								<div class="modal-body">
								<input type="hidden" id="temp_ID" name="temp_ID" >
	 									<p>Are you sure to delete this merchant product ?</p>
								</div>
			              <div class="modal-footer">
			                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
			                <button type="submit" id="btnDelete" name="btnDelete" class="btn btn-outline" >Delete</button>
			              </div>
			            </div>
			            <!-- /.modal-content -->
			          </div>
			          <!-- /.modal-dialog -->
			        </div>
			        <!-- /.modal -->
			      </div>
			    <!--modal Delete -->
				
				<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-gg-small pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> ASSIGN PRODUCTS</button>
				<button <c:out value="${buttonstatus}"/> id="btnDeleteMerchantProduct" name="btnDeleteMerchantProduct" type="button" class="btn btn-gg-small2 pull-left" data-toggle="modal"
					title="Delete"
					data-target="#ModalDelete"
					<%-- data-lid='<c:out value="${transaction.transactionID}" />' --%>
				>
				<!-- <i class="fa fa-trash-o"></i> -->
				<span class="glyphicon glyphicon-trash"></span>
				DELETE
				</button>
				<br><br><br>
				<!-- <div id="dvnewLine"><br><br></div> -->
				
				<table id="tb_product_merchant" class="table table-bordered table-striped table-hover">
								<thead style="color:#7d0d08;">
									<!-- background-color: #d2d6de; -->
									<tr>
										<th></th>
										<th>PRODUCT ID</th>
										<th>PRODUCT NAME</th>
										<th>MERCHANT ID</th>
										<th>MERCHANT NAME</th>
										<th>MERCHANT TYPE</th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listmerchantproduct}" var="merchantproduct">
										<tr>
											<td>
											<!-- <a href=""  -->
											<!-- data-toggle="modal"  -->
											<!-- data-target="#ModalDelete"  -->
											<%-- data-lproductid='<c:out value="${merchantproduct.productID}" />' --%>
											<%-- data-lmerchantid='<c:out value="${merchantproduct.merchantID}" />' --%>
											<!-- >Delete</a> -->
											<div class="checkbox" id="cbMerchantProduct">
											<label><input <c:out value="${buttonstatus}"/> 
												type="checkbox"
												class="icheckbox_minimal-red"
												id="<c:out value="${merchantproduct.productID}" />&&<c:out value="${merchantproduct.merchantID}" />"
												name="<c:out value="${merchantproduct.productID}" />&&<c:out value="${merchantproduct.merchantID}" />"
												value="<c:out value="${merchantproduct.productID}" />&&<c:out value="${merchantproduct.merchantID}" />"
												>
											</label>
											</div>
											</td>
											<td>
											<c:out value="${merchantproduct.productID}" />
											</td>
											<td><c:out value="${merchantproduct.productName}" /></td>
											<td><c:out value="${merchantproduct.merchantID}" /></td>
											<td><c:out value="${merchantproduct.merchantName}" /></td>
											<td><c:out value="${merchantproduct.merchantType}" /></td>
										</tr>

									</c:forEach>						
								</tbody>
							</table> 
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>
<!-- iCheck 1.0.1 -->
<!-- <script src="mainform/plugins/iCheck/icheck.min.js"></script> -->

<script>
$(function() {
	
	$("#tb_product_merchant").DataTable({
		"aaSorting": []
	});
	$("#tb_product").DataTable();
	$('#M006').addClass('active');
	$('#M011').addClass('active');
	//shortcut for button 'new'
	Mousetrap.bind('n', function() {
		FuncButtonNew(), $('#ModalUpdateInsert').modal('show')
	});
	$("#dvErrorAlert").hide();
	$("#btnDeleteMerchantProduct").hide();
// 	$("#dvnewLine").hide();
	
	//Red color scheme for iCheck
//     $('input[type="checkbox"].minimal-red').iCheck({
//       checkboxClass: 'icheckbox_minimal-red'
//     });
});

function FuncClear() {
	$('#mrkProductID').hide();
	$('#mrkBrand').hide();
	$('#mrkMerchant').hide();

	$('#dvProductID').removeClass('has-error');
	$('#dvBrand').removeClass('has-error');
	$('#dvMerchant').removeClass('has-error');
}

function FuncButtonNew() {
	$('#txtProductID').val('');
	document.getElementById("lblProductName").innerHTML = "";
// 	$('#cbbrand').removeAttr('checked');
	$('#slBrand').find('option').remove();
	$("#MerchantPanel").find('label').remove();
//  $('#slBrand').val('');

	document.getElementById("lblTitleModalUpdateInsert").innerHTML = "Merchant Product - ADD NEW";

	FuncClear();
}

function FuncPassStringProduct(lParamProductID,lParamProductName){
	$("#txtProductID").val(lParamProductID);
	document.getElementById("lblProductName").innerHTML = '(' + lParamProductName + ')';
	$('#slBrand').find('option').remove();
// 	$("#MerchantPanel").find('label').remove();
	
	FuncShowBrand();
}

$('#ModalUpdateInsert').on(
		'shown.bs.modal',
		function(event) {
			$("#dvErrorAlert").hide();
			
			var button = $(event.relatedTarget);
			var modal = $(this);

// 			$("#BrandPanel :input").prop("checked", false);

// 				var lbrandlist = lbrand.toString().split(",");
// 				$.each(lbrandlist, function(i) {
// 					modal.find(
// 							".modal-body #" + lbrandlist[i].toString())
// 							.prop('checked', true);
// 				});

			$('#txtProductID').focus();
	 		$('#txtProductID').click();
		})
		
$('#ModalDelete').on('show.bs.modal', function (event) {
	var checkedMerchantProduct = new Array();
	$('#cbMerchantProduct input:checked').each(function() {
		checkedMerchantProduct.push(this.value);
	});
	
		$("#temp_ID").val(checkedMerchantProduct);
	})
		
function FuncValShowBrand(){	
	var ProductID = document.getElementById('txtProductID').value;

	FuncClear();

	if(!ProductID.match(/\S/)) {    	
	    	$('#txtProductID').focus();
	    	$('#dvProductID').addClass('has-error');
	    	$('#mrkProductID').show();
	    	
	    	alert("Fill Product First ...!!!");
	        return false;
	    }
	
	    return true;
}

// $("#slBrand").change(function () {
//     var end = this.value;
//     var firstDropVal = $('#pick').val();
// });

function FuncShowMerchant(){
	$("#MerchantPanel").find('label').remove();
	
	var param = document.getElementById('slBrand').value;
	var paramproduct = document.getElementById('txtProductID').value;
// 	var paramFilterMerchantName = document.getElementById('txtFilterMerchantName').value;
// 	var paramFilterMerchantType = document.getElementById('txtFilterMerchantType').value;
	
	$('#dynamic-content').html(''); // leave this div blank
	
	$.ajax({
        url: '${pageContext.request.contextPath}/getmerchantbybrand',
        type: 'POST',
        data: {brandid : param, productid : paramproduct}, 
//merchantname:paramFilterMerchantName, merchanttype:paramFilterMerchantType},
        dataType: 'html'
   })
   .done(function(data){
        console.log(data); 
        $('#dynamic-content').html(''); // blank before load.
        $('#dynamic-content').html(data); // load here
//         $('#modal-loader').hide(); // hide loader  
   })
   .fail(function(){
        $('#dynamic-content').html('<i class="glyphicon glyphicon-info-sign"></i> Something went wrong, Please try again...');
//         $('#modal-loader').hide();
   });
}

$('#txtFilterMerchant').on('keyup', function() {
    var queryMerchant = this.value;

    $('[id^="chk"]').each(function(i, elem) {
          if (elem.name.indexOf(queryMerchant) != -1) {
        	  $(this).closest('label').show();
			  //elem.style.display = 'block';
          }else{
        	  $(this).closest('label').hide();
			  // elem.style.display = 'none';
          }
    });
});

function FuncSelectAll() {
	$("#MerchantPanel :input").prop("checked", true);
}

function FuncUnselectAll() {
	$("#MerchantPanel :input").prop("checked", false);
}

function FuncValEmptyInput(lParambtn) {
		var ProductID = document.getElementById('txtProductID').value;
		var BrandID = document.getElementById('slBrand').value;
		
		var checkedMerchant = new Array();
		$('#cbMerchant input:checked').each(function() {
			checkedMerchant.push(this.value);
 	});
		
		FuncClear();

	    if(!ProductID.match(/\S/)) {
	    	$("#txtProductID").focus();
	    	$('#dvProductID').addClass('has-error');
	    	$('#mrkProductID').show();
	        return false;
	    } 
	    
	    if(!BrandID.match(/\S/)) {    	
	    	$('#slBrand').focus();
	    	$('#dvBrand').addClass('has-error');
	    	$('#mrkBrand').show();
	        return false;
	    } 
	   
	   if(checkedMerchant.length == 0){
		  $('#MerchantPanel').focus();
		  $('#dvMerchant').addClass('has-error');
		  $('#mrkMerchant').show();
		  return false;
		}
	    
	    jQuery.ajax({
	        url:'${pageContext.request.contextPath}/product_merchant',	
	        type:'POST',
	        data:{"key":lParambtn,"ProductID":ProductID,"BrandID":BrandID,"listMerchant":checkedMerchant},
	        dataType : 'text',
	        success:function(data, textStatus, jqXHR){
	        	if(data.split("--")[0] == 'FailedInsertMerchantProduct')
	        	{
	        		$("#dvErrorAlert").show();
	        		document.getElementById("lblAlert").innerHTML = "Insert Merchant Product Failed";
	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
	        		$("#txtProductID").focus();
	        		return false;
	        	}
	        	else
	        	{
	        		var url = '${pageContext.request.contextPath}/product_merchant';  
		        	$(location).attr('href', url);
	        	}
	        },
	        error:function(data, textStatus, jqXHR){
	            console.log('Service call failed!');
	        }
	    });
	    return true;
	}
	
</script>

<!-- get brand from product -->
<script>
	function FuncShowBrand(){
		var param = document.getElementById('txtProductID').value;
	     
        $.ajax({
             url: '${pageContext.request.contextPath}/getbrand',
             type: 'POST',
             data: {productid : param},
             dataType: 'json'
        })
        .done(function(data){
             console.log(data);
		
		      //for json data type
// 		      $('#slBrand').append("<option></option>");
		      for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slBrand').append("<option value=\"" + key + "\">"+ key + " - " + val + "</option>");
		        
		    }

		  		FuncShowMerchant();  
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
	}
	
	//click merchant product checkbox action
	$(document).ready(function(){
	
	    $(document).on('click', '#cbMerchantProduct', function(e){
    	
	    	var checkedMerchantProduct = new Array();
	    	$('#cbMerchantProduct input:checked').each(function() {
	    		checkedMerchantProduct.push(this.value);
	    	});
	    	if(checkedMerchantProduct.length == 0){
	    		$("#btnDeleteMerchantProduct").hide();
// 	    		$("#dvnewLine").hide();
	    	}
	    	else{
	    		$("#btnDeleteMerchantProduct").show();
// 	    		$("#dvnewLine").show();	
	    	}
	    	
	    });
	});
	
// 	$(document).ready(function(){
		
// 	    $(document).on('click', '#slBrand', function(e){
	    	
	    	
// 	    	if($('#slBrand').find('option').length > 0)
// 	    	{
	    					
// 	    	}
// 	    	else
// 	    	{
//  	    	e.preventDefault();
//  	    	var param = document.getElementById('txtProductID').value;
 	     
// 		         $.ajax({
// 		              url: '${pageContext.request.contextPath}/getbrand',
// 		              type: 'POST',
// 		              data: {productid : param},
// 		              dataType: 'json'
// 		         })
// 		         .done(function(data){
// 		              console.log(data);
 				
//  				      //for json data type
//  				      $('#slBrand').append("<option></option>");
//  				      for (var i in data) {
//  				        var obj = data[i];
//  				        var index = 0;
//  				        var key, val;
//  				        for (var prop in obj) {
//  				            switch (index++) {
//  				                case 0:
//  				                    key = obj[prop];
//  				                    break;
//  				                case 1:
//  				                    val = obj[prop];
//  				                    break;
//  				                default:
//  				                    break;
//  				            }
//  				        }
 				        
//  				        $('#slBrand').append("<option value=\"" + key + "\">" + val + "</option>");
 				        
//  				    }
 				             
//  				         })
//  				         .fail(function(){
//  				        	 console.log('Service call failed!');
//  				         });
// 	    	}
	         
// 	    });
// 	});
	</script>
					
</body>
</html>