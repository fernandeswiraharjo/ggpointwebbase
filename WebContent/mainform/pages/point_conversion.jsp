<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | Point Conversion</title>
<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		<label style="color:#f6f6f6">POINT CONVERSION</label>
	</h1>
	</section>
	
	<!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				<c:if test="${condition == 'SuccessInsertPointConversion'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Insert Point Conversion Success.
        				</div>
 					</c:if>
<%--  					<c:if test="${condition == 'FailedInsert'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Insert Point Conversion Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
					<c:if test="${condition == 'SuccessUpdatePointConversion'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Update Point Conversion Success.
        				</div>
 					</c:if>
<%--  					<c:if test="${condition == 'FailedUpdate'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Update Point Conversion Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
					
					<!-- modal update insert merchant -->
					<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							
							<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
	     					</div>
	     					
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalUpdateInsert" name="lblTitleModalUpdateInsert"></label></h4>
								</div>
   								<div class="modal-body">
   								
       								<div id="dvBrand" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">BRAND</label><label id="mrkBrand" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<select id="slBrand" name="slBrand" class="form-control">
										<c:forEach items="${listbrand}" var="brand">
											<option value="<c:out value="${brand.brandID}" />"><c:out value="${brand.brandID}" /> - <c:out value="${brand.brandName}" /></option>
										</c:forEach>
							        	</select>
       								</div>
       								<div id="dvPrice" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">PRICE</label><label id="mrkPrice" for="recipient-name" class="control-label"> <small>*</small></label>
         								<div class="input-group">
         								<span class="input-group-addon">Rp</span>
         								<input class="form-control number" id="txtPrice" name="txtPrice" placeholder="Price amount for 1 point">
         								<span class="input-group-addon">,00</span>
         								</div>
       								</div>
       								
       								<div class="row"></div>	        								
  								</div>
  								
  								<div class="modal-footer">
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
    									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  								</div>
							</div>
						</div>
					</div>
					<!-- /.end modal update & Insert -->
					
					<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-gg-small pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> ADD NEW</button>
					<br><br><br>
							<table id="tb_point_conversion" class="table table-bordered table-striped table-hover">
								<thead style="color:#7d0d08;">
									<!-- background-color: #d2d6de; -->
									<tr>
										<th>BRAND ID</th>
										<th>POINT</th>
										<th>PRICE</th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listpointconversion}" var="pointconversion">
										<tr>
											<td>
											<a href="" style="color:#144471;font-weight: bold;"
											onclick="FuncUpdate()"
											data-toggle="modal" 
											data-target="#ModalUpdateInsert"
											data-brandid='<c:out value="${pointconversion.brandID}" />'
											data-price='<c:out value="${pointconversion.price}" />'
											>
											<c:out value="${pointconversion.brandID}" />
											</a>	
											</td>
											<td><c:out value="${pointconversion.point}" /></td>
											<td><c:out value="${pointconversion.sPrice}" /></td>
										</tr>

									</c:forEach>						
								</tbody>
							</table> 
							
					</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
					</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
      
    </section>
    <!-- /.content -->
	
	</div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<script>
 	$(function () {
 		$("#tb_point_conversion").DataTable();
  		$('#M003').addClass('active');
  		//shortcut for button 'new'
  	    Mousetrap.bind('n', function() {
  	    	FuncButtonNew(),
  	    	$('#ModalUpdateInsert').modal('show')
  	    	});
  	  $("#dvErrorAlert").hide();
  	});
 	
 	function FuncClear(){
 		$('#mrkBrand').hide();
 		$('#mrkPrice').hide();
 		
 		$('#dvBrand').removeClass('has-error');
 		$('#dvPrice').removeClass('has-error');
 	}
 	
 	function FuncButtonNew() {
 		$('#slBrand').val('');
 		$('#txtPrice').val('');
 		
 		$('#btnSave').show();
 		$('#btnUpdate').hide();
 		$('#slBrand').prop('disabled', false);
 		document.getElementById("lblTitleModalUpdateInsert").innerHTML = "POINT CONVERSION - ADD NEW";	

 		FuncClear();
 	}
 	
 	function FuncUpdate() {
 		$('#btnSave').hide();
 		$('#btnUpdate').show();
 		$('#slBrand').prop('disabled', true);
 		document.getElementById('lblTitleModalUpdateInsert').innerHTML = "POINT CONVERSION - EDIT";

 		FuncClear();
	}
 	
 	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {
 		  $("#dvErrorAlert").hide();
 		  
		  var button = $(event.relatedTarget) // Button that triggered the modal
		  
		  var brandid = button.data('brandid') // Extract info from data-* attributes
		  var price = button.data('price') // Extract info from data-* attributes
		  
		  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
		  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
		  var modal = $(this)

		  modal.find('.modal-body #slBrand').val(brandid)
		  modal.find('.modal-body #txtPrice').val(price)
		  
		  $('#slBrand').focus();
		})
		
	function FuncValEmptyInput(lParambtn) {
 		var BrandID = document.getElementById('slBrand').value;
 		var Price = document.getElementById('txtPrice').value;
 		
 		FuncClear();

 	    if(!BrandID.match(/\S/)) {
 	    	$("#slBrand").focus();
 	    	$('#dvBrand').addClass('has-error');
 	    	$('#mrkBrand').show();
 	        return false;
 	    } 
 	    
 	    if(!Price.match(/\S/)) {    	
 	    	$('#txtPrice').focus();
 	    	$('#dvPrice').addClass('has-error');
 	    	$('#mrkPrice').show();
 	        return false;
 	    } 
 	    
 	    jQuery.ajax({
 	        url:'${pageContext.request.contextPath}/point_conversion',	
 	        type:'POST',
 	        data:{"key":lParambtn,"BrandID":BrandID,"Price":Price},
 	        dataType : 'text',
 	        success:function(data, textStatus, jqXHR){
 	        	if(data.split("--")[0] == 'FailedInsertPointConversion')
 	        	{
 	        		$("#dvErrorAlert").show();
 	        		document.getElementById("lblAlert").innerHTML = "Insert Point Conversion Failed";
 	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
 	        		$("#slBrand").focus();
 	        		return false;
 	        	}
 	        	else if(data.split("--")[0] == 'FailedUpdatePointConversion')
 	        	{
 	        		$("#dvErrorAlert").show();
 	        		document.getElementById("lblAlert").innerHTML = "Update Point Conversion Failed";
 	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
 	        		$("#txtPrice").focus();
 	        		return false;
 	        	}
 	        	else
 	        	{
 	        		var url = '${pageContext.request.contextPath}/point_conversion';  
 	 	        	$(location).attr('href', url);
 	        	}
 	        },
 	        error:function(data, textStatus, jqXHR){
 	            console.log('Service call failed!');
 	        }
 	    });
 	    return true;
 	}
 	
 	$('input.number').keyup(function(event) {

 		  // skip for arrow keys
 		  if(event.which >= 37 && event.which <= 40) return;

 		  // format number
 		  $(this).val(function(index, value) {
 		    return value
 		    .replace(/\D/g, "")
 		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
 		    ;
 		  });
 		});
</script>
	
</body>
</html>