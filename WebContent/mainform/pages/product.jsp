<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | product</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, product-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Select2 -->
<link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="Product" name="Product"
		action="${pageContext.request.contextPath}/product" method="post">

		<div class="wrapper">
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					<label style="color: #f6f6f6">Product</label>
				</h1>
				</section>

				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">

							<div class="box-body">
								<c:if test="${condition == 'SuccessInsertProduct'}">
									<div class="alert alert-success alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-check"></i> Success
										</h4>
										Insert Success.
									</div>
								</c:if>
<%-- 								<c:if test="${condition == 'FailedInsert'}"> --%>
<!-- 									<div class="alert alert-danger alert-dismissible"> -->
<!-- 										<button type="button" class="close" data-dismiss="alert" -->
<!-- 											aria-hidden="true">&times;</button> -->
<!-- 										<h4> -->
<!-- 											<i class="icon fa fa-ban"></i> Failed -->
<!-- 										</h4> -->
<!-- 										Insert Failed. -->
<%-- 										<c:out value="${errorDescription}" /> --%>
<!-- 										. -->
<!-- 									</div> -->
<%-- 								</c:if> --%>
								<c:if test="${condition == 'SuccessUpdateProduct'}">
									<div class="alert alert-success alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-check"></i> Success
										</h4>
										Update Success.
									</div>
								</c:if>
<%-- 								<c:if test="${condition == 'FailedUpdate'}"> --%>
<!-- 									<div class="alert alert-danger alert-dismissible"> -->
<!-- 										<button type="button" class="close" data-dismiss="alert" -->
<!-- 											aria-hidden="true">&times;</button> -->
<!-- 										<h4> -->
<!-- 											<i class="icon fa fa-ban"></i> Failed -->
<!-- 										</h4> -->
<!-- 										Update Failed. -->
<%-- 										<c:out value="${errorDescription}" /> --%>
<!-- 										. -->
<!-- 									</div> -->
<%-- 								</c:if> --%>
								<c:if test="${condition == 'SuccessDelete'}">
									<div class="alert alert-success alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-check"></i> Success
										</h4>
										Delete Success.
									</div>
								</c:if>
								<c:if test="${condition == 'FailedDelete'}">
									<div class="alert alert-danger alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-ban"></i> Failed
										</h4>
										Delete Failed.
										<c:out value="${errorDescription}" />
										.
									</div>
								</c:if>


								<!-- modal update insert Product -->
								<div class="modal fade" id="ModalUpdateInsert" role="dialog"
									aria-labelledby="exampleModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
										
										<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
				          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
				          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
				     					</div>
										
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="exampleModalLabel">
													<label id="lblTitleModalUpdateInsert"
														name="lblTitleModalUpdateInsert"></label>
												</h4>
											</div>
											<div class="modal-body">

										

									
												
												<div id="dvProductID" class="form-group col-xs-6">
													<label for="recipient-name" class="control-label">PRODUCT
														ID</label><label id="mrkProductID" for="recipient-name"
														class="control-label"><small>*</small></label> 
														<input
														type="text" class="form-control" id="txtProductID"
														name="txtProductID" onkeyup="FuncGenerateBarcode()">
												</div>
												<div class="form-group col-xs-6">
												<svg id="code128" name="code128" style="width: 100%; height:100px"></svg>
												</div>
												<div id="dvBrand" class="form-group col-xs-12">
													<label for="message-text" class="control-label">BRAND
														ID</label><label id="mrkBrand" for="recipient-name"
														class="control-label"> <small>*</small>
													</label> <select id="slBrand" name="slBrand"
														class="form-control select2" multiple="multiple"
														data-placeholder="Select Brand" style="width: 100%;">
														<c:forEach items="${listbrand}" var="brand">
															<option id="optbrand" name="optbrand"
																value="<c:out value="${brand.brandID}" />"><c:out
																	value="${brand.brandID}" /> - <c:out
																	value="${brand.brandName}" /></option>
														</c:forEach>
													</select>

												</div>
												<div id="dvProductName" class="form-group col-xs-12">
													<label for="recipient-name" class="control-label">PRODUCT
														NAME</label><label id="mrkProductName" for="recipient-name"
														class="control-label"><small>*</small></label> <input
														type="text" class="form-control" id="txtProductName"
														name="txtProductName">
												</div>
												<div id="dvPrice" class="form-group col-xs-12">
													<label for="message-text" class="control-label">PRICE</label><label
														id="mrkPrice" for="recipient-name" class="control-label"><small>*</small></label>
													<div class="input-group">
														<span class="input-group-addon">Rp</span> 
														<input id="txtPrice" name="txtPrice" 
															class="form-control number" >
														<span class="input-group-addon">,00</span>
													</div>
												</div>
												<!-- 												<div id="dvPoint" class="form-group col-xs-12"> -->
												<!-- 													<label for="message-text" class="control-label">POINT</label><label -->
												<!-- 														id="mrkPoint" for="recipient-name" class="control-label"><small>*</small></label> -->
												<!-- 													<input type="number" class="form-control" id="txtPoint" -->
												<!-- 														name="txtPoint" readonly> -->
												<!-- 												</div> -->
												<div id="dvStatus" class="form-group col-xs-12">
													<label for="message-text" class="control-label">STATUS</label><label
														id="mrkStatus" for="recipient-name" class="control-label">
														<small>*</small>
													</label> <select id="slStatus" name="slStatus" class="form-control"
														style="width: 100%;">
														<option id="optStatus" name="optStatus" value="1" >Active</option>
														<option id="optStatus" name="optStatus" value="0">Non Active</option>
													</select>
												</div>

												<div class="row"></div>
											</div>

											<div class="modal-footer">
												<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnSave"
													name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
												<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small"
													id="btnUpdate" name="btnUpdate"
													onclick="FuncValEmptyInput('update')">Update</button>
												<button type="button" class="btn btn-default"
													data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- /.end modal update & Insert -->

								<!--modal Delete -->
								<div class="example-modal">
									<div class="modal modal-danger" id="ModalDelete" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title">Alert Delete Product</h4>
												</div>
												<div class="modal-body">
													<input type="hidden" id="temp_txtProductId"
														name="temp_txtProductId">
													<!-- 													<input type="hidden" id="temp_txtBrandId" -->
													<!-- 														name="temp_txtBrandId"> -->
													<p>Are you sure to delete this Product document ?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-outline pull-left"
														data-dismiss="modal">Close</button>
													<button type="submit" id="btnDelete" name="btnDelete"
														class="btn btn-outline">Delete</button>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
									<!-- /.modal -->
								</div>
								<!--modal Delete -->

								<div class="col-xs-12">
									<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button"
										class="btn btn-gg-small pull-right" data-toggle="modal"
										data-target="#ModalUpdateInsert" onclick="FuncButtonNew()">
										<i class="fa fa-plus-circle"></i> ADD NEW
									</button>
								</div>
								<div class="col-xs-12">
									<br>
								</div>
								<div class="col-xs-12">
									
									<table id="tb_Product"
										class="table table-bordered table-striped table-hover">
										<thead style="color: #7d0d08;">
											<!-- background-color: #d2d6de; -->
											<tr>
												<th>Product Id</th>
												<th>Product Name</th>
												<th>Price</th>
												<th>Status</th>
<!-- 												<th></th> -->
											</tr>
										</thead>

										<tbody>

											<c:forEach items="${listproduct}" var="product">
												<tr>
													<td><a href=""
														style="color: #144471; font-weight: bold;"
														onclick="FuncButtonUpdate()" data-toggle="modal"
														data-target="#ModalUpdateInsert"
														data-lproductid='<c:out value="${product.productID}" />'
														data-lproductname='<c:out value="${product.productName}" />'
														data-lprice='<c:out value="${product.price}" />'
														data-lstatus='<c:out value="${product.status}" />'> 
														<c:out value="${product.productID}" />
													</a>
													</td>
													<td>
													<a href=""
														style="color: #144471; font-weight: bold;"
														onclick="FuncButtonUpdate()" data-toggle="modal"
														data-target="#ModalUpdateInsert"
														data-lproductid='<c:out value="${product.productID}" />'
														data-lproductname='<c:out value="${product.productName}" />'
														data-lprice='<c:out value="${product.price}" />'
														data-lstatus='<c:out value="${product.status}" />'>
														<c:out value="${product.productName}" />
													</a>
													</td>
													<td><c:out value="${product.price}" /></td>

													<td><c:if test="${product.status == '0'}">
													  Non Active
													</c:if> <c:if test="${product.status == '1'}">
													  Active
													</c:if></td>
<!-- 													<td><a href="" data-toggle="modal" -->
<!-- 														data-target="#ModalDelete" -->
<%-- 														data-lproductid='<c:out value="${product.productID}" />'>Delete</a></td> --%>
												</tr>

											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col-xs-12 -->
				</div>
				<!-- /.row --> </section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->

			<%@ include file="/mainform/pages/master_footer.jsp"%>
		</div>
		<!-- ./wrapper -->

	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<script src="mainform/plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="mainform/dist/js/pages/dashboard2.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>
	<script src="mainform/dist/js/barcode/JsBarcode.all.min.js"></script>



	<script>
		$(function() {
			//Initialize Select2 Elements
			$(".select2").select2();

			// 			$('#slMerchantId').on("change", function (event) {
			// 				FuncLoadMerchantbyBrand();
			// 			})

			// 			//Load Merchant by Brand
			// 			FuncLoadMerchantbyBrand() 

			$("#tb_Product").DataTable({
				"scrollX" : true
			});

			$('#M006').addClass('active');
			$('#M010').addClass('active');
			//shortcut for button 'new'
			Mousetrap.bind('n', function() {
				FuncButtonNew(), $('#ModalUpdateInsert').modal('show')
			});
			$("#dvErrorAlert").hide();
		});

		function FuncClear() {
			$('#mrkProductID').hide();
			$('#mrkProductName').hide();
			$('#mrkPrice').hide();
			$('#mrkPoint').hide();
			$('#mrkBrand').hide();
			$('#mrkStatus').hide();


			$('#dvProductID').removeClass('has-error');
			$('#dvproductName').removeClass('has-error');
			$('#dvPrice').removeClass('has-error');
			$('#dvPoint').removeClass('has-error');
			$('#dvBrand').removeClass('has-error');
			$('#dvStatus').removeClass('has-error');
		}

		function FuncButtonNew() {
			$('#txtProductID').val('');
			$('#txtProductName').val('');
			$('#txtPrice').val('');
			$('#txtPoint').val('');
			$('#slStatus option[value=1]').prop('selected',true);
			$('#slBrand').val(null).trigger("change");

			$('#btnSave').show();
			$('#btnUpdate').hide();
			document.getElementById("lblTitleModalUpdateInsert").innerHTML = "Product - ADD NEW";

			FuncClear();
		}

		function FuncButtonUpdate() {
			$('#btnSave').hide();
			$('#btnUpdate').show();
			document.getElementById("lblTitleModalUpdateInsert").innerHTML = 'Edit Product';

			FuncClear();
			$('#txtproductID').prop('disabled', true);
		}

		function FuncValEmptyInput(lParambtn) {
			var txtProductID = $('#txtProductID').val();
			var txtProductName = $('#txtProductName').val();
			var txtPrice = $('#txtPrice').val();
			var slStatus = $('#slStatus').val();
			var slBrandId = $("#slBrand").val();

			FuncClear();

			if (lParambtn == 'save') {
				$('#txtProductID').prop('disabled', false);
			} else {
				$('#txtProductID').prop('disabled', true);
			}

			if (!txtProductID.match(/\S/)) {
				$("#txtProductID").focus();
				$('#dvProductID').addClass('has-error');
				$('#mrkProductID').show();
				return false;
			}

			if (!txtProductName.match(/\S/)) {
				$("#txtProductName").focus();
				$('#dvProductName').addClass('has-error');
				$('#mrkProductName').show();
				return false;
			}

			if (!txtPrice.match(/\S/)) {
				$("#txtPrice").focus();
				$('#dvPrice').addClass('has-error');
				$('#mrkPrice').show();
				return false;
			}

			if (slStatus.length == 0) {
				$("#slStatus").focus();
				$('#dvStatus').addClass('has-error');
				$('#mrkStatus').show();
				return false;
			}

			if (slBrandId == null) {
				$("#slBrand").focus();
				$('#dvBrand').addClass('has-error');
				$('#mrkBrand').show();
				return false;
			}

			// 			if (slMerchantId.length == 0) {
			// 				$("#slMerchantId").focus();
			// 				$('#dvMerchantId').addClass('has-error');
			// 				$('#mrkMerchantId').show();
			// 				return false;
			// 			}

			//Get list brand from checkboxlist
			// 			var checkedBrand = new Array();
			// 			$('#cbbrand input:checked').each(function() {
			// 				checkedBrand.push(this.value);
			// 			});

			// 			if (checkedBrand.length == 0) {
			// 				$('#dvBrand').addClass('has-error');
			// 				$('#mrkBrand').show();
			// 				return false;
			// 			}

			jQuery.ajax({
				url : '${pageContext.request.contextPath}/product',
				type : 'POST',
				data : {
					"key" : lParambtn,
					"txtProductID" : txtProductID,
					"txtProductName" : txtProductName,
					"txtPrice" : txtPrice,
					"slBrandId" : slBrandId,
					"slStatus" : slStatus

				},
				dataType : 'text',
				success : function(data, textStatus, jqXHR) {
					if(data.split("--")[0] == 'FailedInsertProduct')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Insert Failed";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtProductID").focus();
		        		return false;
		        	}
		        	else if(data.split("--")[0] == 'FailedUpdateProduct')
		        	{
		        		$("#dvErrorAlert").show();
		        		document.getElementById("lblAlert").innerHTML = "Update Failed";
		        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
		        		$("#txtProductName").focus();
		        		return false;
		        	}
		        	else
		        	{
		        		var url = '${pageContext.request.contextPath}/product';
						$(location).attr('href', url);
		        	}
				},
				error : function(data, textStatus, jqXHR) {
					console.log('Service call failed!');
				}
			});

			return true;
		}
		
		function FuncGenerateBarcode(lproductid) {
			
			var txtProductID = $('#txtProductID').val();
			if(!lproductid == ""){
				txtProductID = lproductid
			}
			
			if(!txtProductID == ""){
				$('#code128').show();
				JsBarcode("#code128", txtProductID);
			}
			else
			{
				$('#code128').hide();
			}

			
			
// 			JsBarcode("#ean-13", "1234567890128", {format: "ean13"});
// 			JsBarcode("#ean-8", "12345670", {format: "ean8"});
// 			JsBarcode("#ean-5", "12345", {format: "ean5"});
// 			JsBarcode("#ean-2", "12", {format: "ean2"});
// 			JsBarcode("#upc-a", "123456789012", {format: "upc"});
// 			JsBarcode("#code39", "Hello", {format: "code39"});
// 			JsBarcode("#itf-14", "1234567890123", {format: "itf14"});
// 			JsBarcode("#msi", "123456", {format: "msi"});
// 			JsBarcode("#pharmacode", "12345", {format: "pharmacode"});
		}
		
		
		$('#txtPrice').keyup(function(event) {

	 		  // skip for arrow keys
	 		  if(event.which >= 37 && event.which <= 40) return;

	 		  // format number
	 		  $(this).val(function(index, value) {
	 		    return value
	 		    .replace(/\D/g, "")
	 		    .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
	 		    ;
	 		  });
	 	});

		$('#slBrand')
				.on(
						"select2:unselecting",
						function(e) {
							
							var lbrand = e.params.args.data.id;
							var lproductid = $('#txtProductID').val();
							var selected_value = $('#slBrand').val();
							//Check Merchant
							$
									.ajax(
											{
												url : '${pageContext.request.contextPath}/productcontroller',
												type : 'POST',
												data : {
													productid : lproductid,
													brand : lbrand,
													command : 'CheckProductMerchantbyBrand'
												},
												dataType : 'json'
											})
									.done(
											function(data) {
												console.log(data);
												var data1 = data[0];
												//Check Disini bener atau tidak
												if (data.MerchantID != "") {
													alert("Failed Delete Brand"
															+ lbrand
															+ ", Because Any Merchant on Brand ...");
													$('#slBrand')
															.val(selected_value)
															.trigger(
																	'change.select2')
												}

											}).fail(function() {
										console.log('Service call failed!');
									})
						}).trigger('change');

		$('#ModalUpdateInsert')
				.on(
						'shown.bs.modal',
						function(event) {
							$("#dvErrorAlert").hide();
							
							var button = $(event.relatedTarget);
							var lproductid = button.data('lproductid');
							var lproductname = button.data('lproductname');
							var lstatus = button.data('lstatus');
							var lbrand = button.data('lbrandid');
							var listbrand = new Array();
							var lprice = button.data('lprice');

							var modal = $(this);
							
							modal.find(".modal-body #txtProductID").removeAttr("disabled");
							//$('#slBrand option[value='+lbrand+']').prop('selected', true);
							FuncGenerateBarcode(lproductid);
							$('#slBrand').val(null).trigger("change");
							$('#slStatus option[value='+lstatus+']').prop('selected',true);
							
							
							modal.find(".modal-body #txtProductID").val(lproductid);
							modal.find(".modal-body #txtProductName").val(lproductname);
							

							if (lproductid == null || lproductid == '')
								$('#txtproductID').focus();

							else {
								lprice = lprice.replace("Rp. ", "").replace(" ,-", "");
								modal.find(".modal-body #txtProductName").focus();
								modal.find(".modal-body #txtProductID").attr("disabled", "disabled");

								$
										.ajax(
												{
													url : '${pageContext.request.contextPath}/productcontroller',
													type : 'POST',
													data : {
														productid : lproductid,
														command : 'Update'
													},
													dataType : 'json'
												})
										.done(
												function(data) {
													console.log(data);
													for ( var i in data) {
														var obj = data[i];
														var index = 0;
														var BrandId, BrandName;
														for ( var prop in obj) {
															switch (index++) {
															case 0:
																BrandId = obj[prop];
																break;
															default:
																break;
															}
														}
														listbrand.push(BrandId);
														$('#slBrand')
																.val(listbrand)
																.trigger(
																		'change.select2')
													}

												})
										.fail(
												function() {
													console
															.log('Service call failed!');
												})

							}
							
							modal.find(".modal-body #txtPrice").val(lprice);
		})

		$('#ModalDelete').on('show.bs.modal', function(event) {
			var button = $(event.relatedTarget);
			var lproductid = button.data('lproductid');
			var lbrandid = button.data('lbrandid');
			$("#temp_txtProductId").val(lproductid);
			$("#temp_txtBrandId").val(lbrandid);

		})

		// 		function funcConvertPoint() {
		// 			var price = $("#txtPrice").val();
		// 			var slbrand = $("#slBrand").val();

		// 			if (price == "" || slbrand == "")
		// 			{
		// 				return false;
		// 			}

		// 			//Get Point
		// 			$.ajax(
		// 							{
		// 								url : '${pageContext.request.contextPath}/productcontroller',
		// 								type : 'POST',
		// 								data : {
		// 									price : price,
		// 									slbrand : slbrand,
		// 									command : 'ConvertPoint'
		// 								},
		// 								dataType : 'json'
		// 							})
		// 					.done(
		// 							function(data) {
		// 								console.log(data);
		// 								var data1 = data[0];
		// 								//Check Disini bener atau tidak
		// 								if (data.RoleProductID != "") {
		// 									$("#txtPoint").val(data["Point"]);

		// 								} else {
		// 									alert("Failed Convert Point, Please Check Log Exception ...");
		// 								}

		// 							}).fail(function() {
		// 						console.log('Service call failed!');
		// 					})
		// 		}

		// 		function FuncLoadMerchantbyBrand() {
		// 			var slbrand = $("#slBrand").val();
		// 			var Command = "LoadMerchantbyBrand";

		// 			$.ajax({
		// 				url : '${pageContext.request.contextPath}/productcontroller',
		// 				type : 'POST',
		// 				data : {
		// 					command : Command,
		// 					slbrand : slbrand
		// 				},
		// 				dataType : 'json'
		// 			})
		// 					.done(
		// 							function(data) {
		// 								console.log(data);
		// 								for ( var i in data) {
		// 									var obj = data[i];
		// 									var index = 0;
		// 									var id, name;
		// 									for ( var prop in obj) {
		// 										switch (index++) {
		// 										case 0:
		// 											id = obj[prop];
		// 											break;
		// 										case 1:
		// 											name = obj[prop];
		// 											break;

		// 										default:
		// 											break;
		// 										}
		// 									}

		// 									if ($("#slMerchantId option[value="
		// 											+ id + "]").length > 0) {
		// 									} else {
		// 										$('#slMerchantId').append(
		// 												"<option id=\"" + id + "\" value=\"" + id + "\">"
		// 														+ name + "</option>");
		// 									}

		// 								}

		// 							}).fail(function() {
		// 						console.log('Service call failed!');
		// 					})
		// 		}
	</script>
</body>
</html>