<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | Register Merchant</title>
<!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">
  
  <!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
	
</head>
<body class="hold-transition skin-blue sidebar-mini">

<%@ include file="/mainform/pages/master_header.jsp" %>

<form id="RegisterMerchant" name="RegisterMerchant" action = "${pageContext.request.contextPath}/register_merchant" method="post" target="_blank">

<div class="wrapper">
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
	<h1>
		<label style="color:#f6f6f6">REGISTER MERCHANT</label>
	</h1>
	</section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
		<div class="col-xs-12">

			<div class="box">

				<div class="box-body">
				<c:if test="${condition == 'SuccessInsertMerchant'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Insert Merchant Success.
        				</div>
 					</c:if>
<%--  					<c:if test="${condition == 'FailedInsert'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Insert Merchant Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
					<c:if test="${condition == 'SuccessUpdateMerchant'}">
				  <div class="alert alert-success alert-dismissible">
    				      	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
       				  	<h4><i class="icon fa fa-check"></i> Success</h4>
          			  	Update Merchant Success.
        				</div>
 					</c:if>
<%--  					<c:if test="${condition == 'FailedUpdate'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Update Merchant Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
<%-- 					<c:if test="${condition == 'FailedExport'}"> --%>
<!--  						<div class="alert alert-danger alert-dismissible"> -->
<!--           				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button> -->
<!--           				<h4><i class="icon fa fa-ban"></i> Failed</h4> -->
<%--           				Export Merchant Failed. <c:out value="${errorDescription}"/>. --%>
<!--         				</div> -->
<%-- 					</c:if> --%>
					
					<!-- modal update insert merchant -->
					<div class="modal fade" id="ModalUpdateInsert" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
							
							<div id="dvErrorAlert" class="alert alert-danger alert-dismissible">
	          				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	          				<h4><i class="icon fa fa-ban"></i> Failed</h4>
	          				<label id="lblAlert"></label>. <label id="lblAlertDescription"></label>.
	     					</div>
			     					
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="exampleModalLabel"><label id="lblTitleModalUpdateInsert" name="lblTitleModalUpdateInsert"></label></h4>
								</div>
   								<div class="modal-body">
   								
   								<label for="recipient-name" class="control-label">Main Info</label>
   								<div class="panel panel-primary" id="MainInfoPanel">
   								<div class="panel-body fixed-panel">
   								
       								<div id="dvStoreID" class="form-group col-xs-6">
	         							<label for="recipient-name" class="control-label">ID STORE</label><label id="mrkStoreID" for="recipient-name" class="control-label"><small>*</small></label>
         								<input type="text" class="form-control" id="txtStoreID" name="txtStoreID">
       								</div>
       								<div id="dvStoreName" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">STORE NAME</label><label id="mrkStoreName" for="recipient-name" class="control-label"><small>*</small></label>	
         								<input type="text" class="form-control" id="txtStoreName" name="txtStoreName">
       								</div>
       								<div id="dvPIC" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">PIC</label><label id="mrkPIC" for="recipient-name" class="control-label"><small>*</small></label>	
         								<input type="text" class="form-control" id="txtPIC" name="txtPIC">
       								</div>
       								<div id="dvPhone" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">PHONE</label><label id="mrkPhone" for="recipient-name" class="control-label"><small>*</small></label>	
         								<input type="text" class="form-control" id="txtPhone" name="txtPhone">
       								</div>
       								<div id="dvEmail" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">EMAIL</label><label id="mrkEmail" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<input type="text" class="form-control" id="txtEmail" name="txtEmail">
       								</div>
       								<div id="dvAddress" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">ADDRESS</label><label id="mrkAddress" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<input type="text" class="form-control" id="txtAddress" name="txtAddress">
       								</div>
       								<div id="dvBrand" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">BRAND</label><label id="mrkBrand" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<select id="slBrand" name="slBrand" class="form-control">
										<c:forEach items="${listbrand}" var="brand">
											<option value="<c:out value="${brand.brandID}" />"><c:out value="${brand.brandID}" /> - <c:out value="${brand.brandName}" /></option>
										</c:forEach>
							        	</select>
<!-- 										<div class="panel panel-primary" id="BrandPanel"> -->
<!-- 										<div class="panel-body fixed-panel"> -->
<%-- 											<c:forEach items="${listbrand}" var="brand"> --%>
<!-- 												<div class="checkbox" id="cbBrand"> -->
<%-- 												<label><input type="checkbox" id="<c:out value="${brand.brandID}" />" name="<c:out value="${brand.brandID}" />" value="<c:out value="${brand.brandID}" />"> --%>
<%-- 												<c:out value="${brand.brandName}" /> --%>
<!-- 												</label> -->
<!-- 												</div> -->
<%-- 											</c:forEach> --%>
<!-- 										</div> -->
<!-- 										</div> -->
       								</div>
       								<div id="dvType" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">TYPE</label><label id="mrkType" for="recipient-name" class="control-label"> <small>*</small></label>
         								<select id="slType" name="slType" class="form-control">
										<c:forEach items="${listtype}" var="type">
											<option value="<c:out value="${type.merchantTypeID}" />"><c:out value="${type.merchantTypeName}" /></option>
										</c:forEach>
							        	</select>
       								</div>
       								
								</div>
								</div>
								<!-- /.MainInfoPanel -->
								
								
								<label for="recipient-name" class="control-label">Area Info</label>
   								<div class="panel panel-primary" id="AreaInfoPanel">
   								<div class="panel-body fixed-panel">
   								
       								<div id="dvArea" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">AREA</label><label id="mrkArea" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<select id="slArea" name="slArea" class="form-control"> 
										<c:forEach items="${listarea}" var="area">
											<option value="<c:out value="${area.areaID}" />"><c:out value="${area.areaID}" /> - <c:out value="${area.areaName}" /></option>
										</c:forEach>
							        	</select>
       								</div>
       								<div id="dvProvince" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">PROVINCE</label><label id="mrkProvince" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<select id="slProvince" name="slProvince" class="form-control" onchange="FuncShowDistrict()">
										<c:forEach items="${listprovince}" var="province">
											<option value="<c:out value="${province.id}" />"><c:out value="${province.nama}" /></option>
										</c:forEach>
							        	</select>
       								</div>
       								<div id="dvDistrict" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">DISTRICT</label><label id="mrkDistrict" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<select id="slDistrict" name="slDistrict" class="form-control" onfocus="FuncValShowDistrict()" onchange="FuncShowSubDistrict()">
							        	</select>
       								</div>
       								<div id="dvSubDistrict" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">SUB-DISTRICT</label><label id="mrkSubDistrict" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<select id="slSubDistrict" name="slSubDistrict" class="form-control" onfocus="FuncValShowSubDistrict()">
							        	</select>
       								</div>
       								
       							</div>
								</div>
								<!-- /.AreaInfoPanel -->
								
								<label for="recipient-name" class="control-label">Banking Info</label>
   								<div class="panel panel-primary" id="BankingInfoPanel">
   								<div class="panel-body fixed-panel">
   								
       								<div id="dvBank" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">BANK</label><label id="mrkBank" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<select id="slBank" name="slBank" class="form-control">
										<c:forEach items="${listbank}" var="bank">
											<option value="<c:out value="${bank.bankID}" />"><c:out value="${bank.bankName}" /> - <c:out value="${bank.bankID}" /></option>
										</c:forEach>
							        	</select>
       								</div>
       								<div id="dvAccountName" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">ACCOUNT NAME</label><label id="mrkAccountName" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<input type="text" class="form-control" id="txtAccountName" name="txtAccountName">
       								</div>
       								<div id="dvAccountNumber" class="form-group col-xs-6">
         								<label for="message-text" class="control-label">ACCOUNT NUMBER</label><label id="mrkAccountNumber" for="recipient-name" class="control-label"> <small>*</small></label>	
         								<input type="text" class="form-control" id="txtAccountNumber" name="txtAccountNumber">
       								</div>
       								
       							</div>
								</div>
								<!-- /.BankingInfoPanel -->
       								
       								
       								<div class="row"></div>	        								
  								</div>
  								
  								<div class="modal-footer">
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnSave" name="btnSave" onclick="FuncValEmptyInput('save')">Save</button>
    									<button <c:out value="${buttonstatus}"/> type="button" class="btn btn-gg-small" id="btnUpdate" name="btnUpdate" onclick="FuncValEmptyInput('update')">Update</button>
    									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
  								</div>
							</div>
						</div>
					</div>
					<!-- /.end modal update & Insert -->
					
					<button <c:out value="${buttonstatus}"/> id="btnModalNew" name="btnModalNew" type="button" class="btn btn-gg-small pull-right" data-toggle="modal" data-target="#ModalUpdateInsert" onclick="FuncButtonNew()"><i class="fa fa-plus-circle"></i> ADD NEW</button>
					<br><br><br>
							<table id="tb_register_merchant" class="table table-bordered table-striped table-hover">
								<thead style="color:#7d0d08;">
									<!-- background-color: #d2d6de; -->
									<tr>
										<th>STORE ID</th>
										<th>NAME</th>
										<th>EMAIL</th>
										<th>CONTACT</th>
										<th>PROVINCE</th>
										<th></th>
									</tr>
								</thead>

								<tbody>

									<c:forEach items="${listmerchant}" var="merchant">
										<tr>
											<td>
											<a href="" id="updatemerchant" name="updatemerchant" style="color:#144471;font-weight: bold;"
											data-toggle="modal" 
											data-target="#ModalUpdateInsert"
											onclick="FuncUpdateMerchant('<c:out value="${merchant.merchantProvinceID}" />','<c:out value="${merchant.merchantDistrict}" />')"
											data-storeid='<c:out value="${merchant.merchantID}" />'
											data-storename='<c:out value="${merchant.merchantName}" />'
											data-pic='<c:out value="${merchant.merchantPIC}" />'
											data-phone='<c:out value="${merchant.merchantContact}" />'
											data-email='<c:out value="${merchant.merchantEmail}" />'
											data-brand='<c:out value="${merchant.merchantBrand}" />'
											data-type='<c:out value="${merchant.merchantType}" />'
											data-area='<c:out value="${merchant.merchantArea}" />'
											data-address='<c:out value="${merchant.merchantAddress}" />'
											data-province='<c:out value="${merchant.merchantProvinceID}" />'
											data-district='<c:out value="${merchant.merchantDistrict}" />'
											data-subdistrict='<c:out value="${merchant.merchantSubDistrict}" />'
											data-bank='<c:out value="${merchant.merchantBank}" />'
											data-accountname='<c:out value="${merchant.accountName}" />'
											data-accountnumber='<c:out value="${merchant.merchantAccountNumber}" />'
											disabled=<c:out value="${buttonstatus}"/>
											>
											<c:out value="${merchant.merchantID}" />
											</a>	
											</td>
											<td>
											<a href="" id="updatemerchant" name="updatemerchant" style="color:#144471;font-weight: bold;"
											data-toggle="modal" 
											data-target="#ModalUpdateInsert"
											onclick="FuncUpdateMerchant('<c:out value="${merchant.merchantProvinceID}" />','<c:out value="${merchant.merchantDistrict}" />')"
											data-storeid='<c:out value="${merchant.merchantID}" />'
											data-storename='<c:out value="${merchant.merchantName}" />'
											data-pic='<c:out value="${merchant.merchantPIC}" />'
											data-phone='<c:out value="${merchant.merchantContact}" />'
											data-email='<c:out value="${merchant.merchantEmail}" />'
											data-brand='<c:out value="${merchant.merchantBrand}" />'
											data-type='<c:out value="${merchant.merchantType}" />'
											data-area='<c:out value="${merchant.merchantArea}" />'
											data-address='<c:out value="${merchant.merchantAddress}" />'
											data-province='<c:out value="${merchant.merchantProvinceID}" />'
											data-district='<c:out value="${merchant.merchantDistrict}" />'
											data-subdistrict='<c:out value="${merchant.merchantSubDistrict}" />'
											data-bank='<c:out value="${merchant.merchantBank}" />'
											data-accountname='<c:out value="${merchant.accountName}" />'
											data-accountnumber='<c:out value="${merchant.merchantAccountNumber}" />'
											>
											<c:out value="${merchant.merchantName}" />
											</a>
											</td>
											<td><c:out value="${merchant.merchantEmail}" /></td>
											<td><c:out value="${merchant.merchantContact}" /></td>
											<td><c:out value="${merchant.merchantProvince}" /></td>
											<td>
							                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown"
							                  onclick="sendvalueLi(
							                    					'pdf',
							                    					'<c:out value="${merchant.merchantAreaName}" />',
							                    					'<c:out value="${merchant.merchantID}" />',
							                    					'<c:out value="${merchant.merchantName}" />',
							                    					'<c:out value="${merchant.merchantTypeName}" />',
							                    					'<c:out value="${merchant.merchantAddress}" />',
							                    					'<c:out value="${merchant.merchantProvince}" />',
							                    					'<c:out value="${merchant.merchantDistrictName}" />',
							                    					'<c:out value="${merchant.merchantSubDistrictName}" />',
							                    					'<c:out value="${merchant.merchantPIC}" />',
							                    					'<c:out value="${merchant.merchantContact}" />',
							                    					'<c:out value="${merchant.merchantBankName}" />',
							                    					'<c:out value="${merchant.accountName}" />',
							                    					'<c:out value="${merchant.merchantAccountNumber}" />'
							                    					)"
							                    >
							                    EXPORT &nbsp
							                    <i class="fa  fa-share-square-o"></i>
							                    </button>
							                    <input type="hidden" id="key" name="key" value="" />
							                    <input type="hidden" id="area" name="area" value="" />
							                    <input type="hidden" id="merchantid" name="merchantid" value="" />
							                    <input type="hidden" id="merchantname" name="merchantname" value="" />
							                    <input type="hidden" id="type" name="type" value="" />
							                    <input type="hidden" id="address" name="address" value="" />
							                    <input type="hidden" id="province" name="province" value="" />
							                    <input type="hidden" id="district" name="district" value="" />
							                    <input type="hidden" id="subdistrict" name="subdistrict" value="" />
							                    <input type="hidden" id="pic" name="pic" value="" />
							                    <input type="hidden" id="contact" name="contact" value="" />
							                    <input type="hidden" id="bank" name="bank" value="" />
							                    <input type="hidden" id="accountname" name="accountname" value="" />
							                    <input type="hidden" id="accountnumber" name="accountnumber" value="" />
              	  							</td>
										</tr>

									</c:forEach>						
								</tbody>
							</table> 
				</div>
				 <!-- /.box-body -->
			</div>
			 <!-- /.box -->
		</div>
		<!-- /.col-xs-12 -->
	</div>
	<!-- /.row -->
      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <%@ include file="/mainform/pages/master_footer.jsp" %>
</div>
<!-- ./wrapper -->

</form>

<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="mainform/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="mainform/dist/js/app.min.js"></script>
<!-- Sparkline -->
<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="mainform/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="mainform/dist/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="mainform/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- MouseTrap for adding shortcut key in this page -->
<script src="mainform/plugins/mousetrap.js"></script>

<script>
 	$(function () {
 		$("#tb_register_merchant").DataTable();
  		$('#M002').addClass('active');
  		//shortcut for button 'new'
  	    Mousetrap.bind('n', function() {
  	    	FuncButtonNew(),
  	    	$('#ModalUpdateInsert').modal('show')
  	    	});
  	    $("#dvErrorAlert").hide();
  	});
 	
 	function FuncClear(){
 		$('#mrkStoreID').hide();
 		$('#mrkStoreName').hide();
 		$('#mrkPIC').hide();
 		$('#mrkPhone').hide();
 		$('#mrkEmail').hide();
 		$('#mrkBrand').hide();
 		$('#mrkType').hide();
 		$('#mrkArea').hide();
 		$('#mrkAddress').hide();
 		$('#mrkProvince').hide();
 		$('#mrkSubDistrict').hide();
 		$('#mrkDistrict').hide();
 		$('#mrkBank').hide();
 		$('#mrkAccountName').hide();
 		$('#mrkAccountNumber').hide();
 		
 		$('#dvStoreID').removeClass('has-error');
 		$('#dvStoreName').removeClass('has-error');
 		$('#dvPIC').removeClass('has-error');
 		$('#dvPhone').removeClass('has-error');
 		$('#dvEmail').removeClass('has-error');
 		$('#dvBrand').removeClass('has-error');
 		$('#dvType').removeClass('has-error');
 		$('#dvArea').removeClass('has-error');
 		$('#dvAddress').removeClass('has-error');
 		$('#dvProvince').removeClass('has-error');
 		$('#dvSubDistrict').removeClass('has-error');
 		$('#dvDistrict').removeClass('has-error');
 		$('#dvBank').removeClass('has-error');
 		$('#dvAccountName').removeClass('has-error');
 		$('#dvAccountNumber').removeClass('has-error');
 	}
 	
 	//for exporting report
 	function sendvalueLi(str,area,merchantid,merchantname,type,address,province,district,subdistrict,pic,contact,bank,accountname,accountnumber) {
			$("#key").val(str);
			$("#area").val(area);
			$("#merchantid").val(merchantid);
			$("#merchantname").val(merchantname);
			$("#type").val(type);
			$("#address").val(address);
			$("#province").val(province);
			$("#district").val(district);
			$("#subdistrict").val(subdistrict);
			$("#pic").val(pic);
			$("#contact").val(contact);
			$("#bank").val(bank);
			$("#accountname").val(accountname);
			$("#accountnumber").val(accountnumber);
			RegisterMerchant.submit();
		}
 	
 	function FuncButtonNew() {
 		$('#txtStoreID').val('');
 		$('#txtStoreName').val('');
 		$('#txtPIC').val('');
 		$('#txtPhone').val('');
 		$('#txtEmail').val('');
 		$('#slBrand').val('');
// 		$("#BrandPanel :input").prop("checked", false);
 		$('#slType').val('');
 		$('#slArea').val('');
 		$('#txtAddress').val('');
 		$('#slProvince').val('');
 		$('#slSubDistrict').val('');
 		$('#slDistrict').val('');
 		$('#slBank').val('');
 		$('#txtAccountName').val('');
 		$('#txtAccountNumber').val('');
 		
 		$('#btnSave').show();
 		$('#btnUpdate').hide();
 		$('#txtStoreID').prop('disabled', false);
 		document.getElementById("lblTitleModalUpdateInsert").innerHTML = "REGISTER MERCHANT - ADD NEW";	

 		FuncClear();
 	}
 	
 	function FuncUpdateMerchant(lProvinceID,lDistrictID) {
 		$('#btnSave').hide();
 		$('#btnUpdate').show();
 		$('#txtStoreID').prop('disabled', true);
 		document.getElementById('lblTitleModalUpdateInsert').innerHTML = "REGISTER MERCHANT - EDIT MERCHANT";

 		FuncClear();
 		
 		$('#slProvince').val(lProvinceID);
 		FuncShowDistrict();
//  		$('#slDistrict').find('option').remove();
//  		$('#slDistrict').click();
 		
 		$('#slSubDistrict').find('option').remove();
 		FuncSlSubDistrictClick(lDistrictID);
 		
//  		$('#slDistrict').val(lDistrictID);
//  		$('#slSubDistrict').find('option').remove();
//  		$('#slSubDistrict').click();
	}
 	
 	$('#ModalUpdateInsert').on('shown.bs.modal', function (event) {	
 		  $("#dvErrorAlert").hide();
 		
 		  var button = $(event.relatedTarget) // Button that triggered the modal
 		  
 		  var storeid = button.data('storeid') // Extract info from data-* attributes
 		  var storename = button.data('storename') // Extract info from data-* attributes
 		  var pic = button.data('pic') // Extract info from data-* attributes
 		  var phone = button.data('phone') // Extract info from data-* attributes
 		  var email = button.data('email') // Extract info from data-* attributes
 		  var brand = button.data('brand') // Extract info from data-* attributes
 		  
//  		  if(button.data('brand')==undefined)
//  			 var brandlist = button.data('brand') // Extract info from data-* attributes
//  		   else
//  		  	 var brandlist = button.data('brand').split(",") // Extract info from data-* attributes
 		  
 		  var area = button.data('area') // Extract info from data-* attributes
 		  var type = button.data('type') // Extract info from data-* attributes
 		  var address = button.data('address') // Extract info from data-* attributes
 		  var province = button.data('province') // Extract info from data-* attributes
 		  var district = button.data('district') // Extract info from data-* attributes
 		  var subdistrict = button.data('subdistrict') // Extract info from data-* attributes
 		  var bank = button.data('bank') // Extract info from data-* attributes
 		  var accountname = button.data('accountname') // Extract info from data-* attributes
 		  var accountnumber = button.data('accountnumber') // Extract info from data-* attributes
 		  
 		  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
 		  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
 		  var modal = $(this)

 		  modal.find('.modal-body #txtStoreID').val(storeid)
 		  modal.find('.modal-body #txtStoreName').val(storename)
 		  modal.find('.modal-body #txtPIC').val(pic)
 		  modal.find('.modal-body #txtPhone').val(phone)
 		  modal.find('.modal-body #txtEmail').val(email)
 		  
//  		  $("#BrandPanel :input").prop("checked", false);
//  		  $.each(brandlist,function(i){
// 		 	 modal.find(".modal-body #"+brandlist[i].toString()).prop('checked', true);
// 		 	 });
 		  
 		  modal.find('.modal-body #slBrand').val(brand)
		  modal.find('.modal-body #slType').val(type)
 		  modal.find('.modal-body #slArea').val(area)
 		  modal.find('.modal-body #slProvince').val(province)
 		  modal.find('.modal-body #slDistrict').val(district)
 		  modal.find('.modal-body #slSubDistrict').val(subdistrict)
 		  modal.find('.modal-body #slBank').val(bank)
 		  modal.find('.modal-body #txtAddress').val(address)
 		  modal.find('.modal-body #txtAccountName').val(accountname)
 		  modal.find('.modal-body #txtAccountNumber').val(accountnumber)
 		  $('#txtStoreID').focus();
 		})
 		
 	function FuncValShowDistrict(){	
		var Province = document.getElementById('slProvince').value;
	
		FuncClear();
	
		if(!Province.match(/\S/)) {    	
		    	$('#slProvince').focus();
		    	$('#dvProvince').addClass('has-error');
		    	$('#mrkProvince').show();
		    	
		    	alert("Fill Province First ...!!!");
		        return false;
		    } 
		    return true;
	}
 	
 	function FuncValShowSubDistrict(){	
 		var District = document.getElementById('slDistrict').value;

 		FuncClear();

 		if(!District.match(/\S/)) {    	
 		    	$('#slDistrict').focus();
 		    	$('#dvDistrict').addClass('has-error');
 		    	$('#mrkDistrict').show();
 		    	
 		    	alert("Fill District First ...!!!");
 		        return false;
 		    } 
 		    return true;
 		}
 	
 	function FuncValEmptyInput(lParambtn) {
 		var StoreID = document.getElementById('txtStoreID').value;
 		var StoreName = document.getElementById('txtStoreName').value;
 		var PIC = document.getElementById('txtPIC').value;
 		var Phone = document.getElementById('txtPhone').value;
 		var Email = document.getElementById('txtEmail').value;
 		var Brand = document.getElementById('slBrand').value;
 		var Type = document.getElementById('slType').value;
 		var Area = document.getElementById('slArea').value;
 		var Address = document.getElementById('txtAddress').value;
 		var Province = document.getElementById('slProvince').value;
 		var District = document.getElementById('slDistrict').value;
 		var SubDistrict = document.getElementById('slSubDistrict').value;
 		var Bank = document.getElementById('slBank').value;
 		var AccountName = document.getElementById('txtAccountName').value;
 		var AccountNumber = document.getElementById('txtAccountNumber').value;
 		
//  		var checkedBrand = new Array();
//  		$('#cbBrand input:checked').each(function() {
//  			checkedBrand.push(this.value);
//      	});
 		
 		FuncClear();

 	    if(!StoreID.match(/\S/)) {
 	    	$("#txtStoreID").focus();
 	    	$('#dvStoreID').addClass('has-error');
 	    	$('#mrkStoreID').show();
 	        return false;
 	    } 
 	    
 	    if(!StoreName.match(/\S/)) {    	
 	    	$('#txtStoreName').focus();
 	    	$('#dvStoreName').addClass('has-error');
 	    	$('#mrkStoreName').show();
 	        return false;
 	    } 
 	    
 	    if(!PIC.match(/\S/)) {
 	    	$('#txtPIC').focus();
 	    	$('#dvPIC').addClass('has-error');
 	    	$('#mrkPIC').show();
 	        return false;
 	    }
 	    
 	    if(!Phone.match(/\S/)) {
 	    	$('#txtPhone').focus();
 	    	$('#dvPhone').addClass('has-error');
 	    	$('#mrkPhone').show();
 	        return false;
 	    }
 	    
 	   if(!Email.match(/\S/)) {
	    	$('#txtEmial').focus();
	    	$('#dvEmail').addClass('has-error');
	    	$('#mrkEmail').show();
	        return false;
	    }
 	   
//  	   if(checkedBrand.length == 0){
//  		  $('#slBrand').focus();
// 		  $('#dvBrand').addClass('has-error');
// 		  $('#mrkBrand').show();
// 		  return false;
//  		}
 	    
 	    if(!Brand.match(/\S/)) {
 	    	$('#slBrand').focus();
 	    	$('#dvBrand').addClass('has-error');
 	    	$('#mrkBrand').show();
 	        return false;
 	    }
 	    
 	   if(!Type.match(/\S/)) {
	    	$('#slType').focus();
	    	$('#dvType').addClass('has-error');
	    	$('#mrkType').show();
	        return false;
	    }
 	   
 	  if(!Area.match(/\S/)) {
	    	$('#slArea').focus();
	    	$('#dvArea').addClass('has-error');
	    	$('#mrkArea').show();
	        return false;
	    }
 	  
 	 if(!Address.match(/\S/)) {
	    	$('#txtAddress').focus();
	    	$('#dvAddress').addClass('has-error');
	    	$('#mrkAddress').show();
	        return false;
	    }
 	 
 	if(!Province.match(/\S/)) {
    	$('#slProvince').focus();
    	$('#dvProvince').addClass('has-error');
    	$('#mrkProvince').show();
        return false;
    }
 	
 	if(!District.match(/\S/)) {
    	$('#slDistrict').focus();
    	$('#dvDistrict').addClass('has-error');
    	$('#mrkDistrict').show();
        return false;
    }
 	
 	if(!SubDistrict.match(/\S/)) {
    	$('#slSubDistrict').focus();
    	$('#dvSubDistrict').addClass('has-error');
    	$('#mrkSubDistrict').show();
        return false;
    }
 	
 	if(!Bank.match(/\S/)) {
    	$('#slBank').focus();
    	$('#dvBank').addClass('has-error');
    	$('#mrkBank').show();
        return false;
    }
 	
 	if(!AccountName.match(/\S/)) {
    	$('#txtAccountName').focus();
    	$('#dvAccountName').addClass('has-error');
    	$('#mrkAccountName').show();
        return false;
    }
 	
 	if(!AccountNumber.match(/\S/)) {
    	$('#txtAccountNumber').focus();
    	$('#dvAccountNumber').addClass('has-error');
    	$('#mrkAccountNumber').show();
        return false;
    }
 	    
 	    jQuery.ajax({
 	        url:'${pageContext.request.contextPath}/register_merchant',	
 	        type:'POST',
 	        data:{"key":lParambtn,"StoreID":StoreID,"StoreName":StoreName,"PIC":PIC,"Phone":Phone,"Email":Email,
 	        	"Type":Type,"Area":Area,"Address":Address,"Province":Province,"District":District,"SubDistrict":SubDistrict,
 	        	"Bank":Bank,"AccountName":AccountName,"AccountNumber":AccountNumber,"Brand":Brand},
//  	        "listBrand":checkedBrand
 	        dataType : 'text',
 	        success:function(data, textStatus, jqXHR){
 	        	if(data.split("--")[0] == 'FailedInsertMerchant')
 	        	{
 	        		$("#dvErrorAlert").show();
 	        		document.getElementById("lblAlert").innerHTML = "Insert Merchant Failed";
 	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
 	        		$("#txtStoreID").focus();
 	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
 	        		return false;
 	        	}
 	        	else if(data.split("--")[0] == 'FailedUpdateMerchant')
 	        	{
 	        		$("#dvErrorAlert").show();
 	        		document.getElementById("lblAlert").innerHTML = "Update Merchant Failed";
 	        		document.getElementById("lblAlertDescription").innerHTML = data.split("--")[1];
 	        		$("#txtStoreName").focus();
 	        		$("#ModalUpdateInsert").animate({scrollTop:0}, 'slow');
 	        		return false;
 	        	}
 	        	else
 	        	{
 	        		var url = '${pageContext.request.contextPath}/register_merchant';  
 	 	        	$(location).attr('href', url);
 	        	}
 	        },
 	        error:function(data, textStatus, jqXHR){
 	            console.log('Service call failed!');
 	        }
 	    });
 	    return true;
 	}
 	</script>
 	
 	<script>
//  	$(document).ready(function(){
 		
//  	    $(document).on('click', '#slProvince', function(e){
//  	    	$('#slDistrict').find('option').remove();
//  	    });
//  	});
 	</script>
 	
 	<!-- get district from province -->
 	<script>
 	function FuncShowDistrict(){	
 		$('#slDistrict').find('option').remove();
 		$('#slSubDistrict').find('option').remove();
 		
   		var param = document.getElementById('slProvince').value;
 	     
         $.ajax({
              url: '${pageContext.request.contextPath}/getdistrict',
              type: 'POST',
              data: {provinceid : param},
              dataType: 'json'
         })
         .done(function(data){
              console.log(data);
		
		      //for json data type
		      for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slDistrict').append("<option value=\"" + key + "\">" + val + "</option>");
		    }
		      
		      FuncShowSubDistrict();
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
 	}
 	
//  	$(document).ready(function(){
 		
//  	    $(document).on('click', '#slDistrict', function(e){
 	    	
//  	    	$('#slSubDistrict').find('option').remove();
 	    	
//  	    	if($('#slDistrict').find('option').length > 0)
//  	    	{
 	    					
//  	    	}
//  	    	else
//  	    	{
// 	 	    	e.preventDefault();
// 	 	    	var param = document.getElementById('slProvince').value;
	 	     
// 			         $.ajax({
// 			              url: '${pageContext.request.contextPath}/getdistrict',
// 			              type: 'POST',
// 			              data: {provinceid : param},
// 			              dataType: 'json'
// 			         })
// 			         .done(function(data){
// 			              console.log(data);
	 				
// 	 				             //for json data type
// 	 				      for (var i in data) {
// 	 				        var obj = data[i];
// 	 				        var index = 0;
// 	 				        var key, val;
// 	 				        for (var prop in obj) {
// 	 				            switch (index++) {
// 	 				                case 0:
// 	 				                    key = obj[prop];
// 	 				                    break;
// 	 				                case 1:
// 	 				                    val = obj[prop];
// 	 				                    break;
// 	 				                default:
// 	 				                    break;
// 	 				            }
// 	 				        }
	 				        
// 	 				        $('#slDistrict').append("<option value=\"" + key + "\">" + val + "</option>");
	 				        
// 	 				    }
	 				             
// 	 				         })
// 	 				         .fail(function(){
// 	 				        	 console.log('Service call failed!');
// 	 				         });
//  	    	}
		         
//  	    });
//  	});
 	</script>
 	
 	<!-- get sub district from district -->
 	<script>
 	function FuncShowSubDistrict(){
 		$('#slSubDistrict').find('option').remove();
 		
 		var param = document.getElementById('slDistrict').value;
	     
        $.ajax({
             url: '${pageContext.request.contextPath}/getsubdistrict',
             type: 'POST',
             data: {districtid : param},
             dataType: 'json'
        })
        .done(function(data){
             console.log(data);
		
		             //for json data type
		      for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slSubDistrict').append("<option value=\"" + key + "\">" + val + "</option>");
		        
		    }
		             
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
 	}
 	
//  	$(document).ready(function(){
 		
//  	    $(document).on('click', '#slSubDistrict', function(e){
 	    	
//  	    	if($('#slSubDistrict').find('option').length > 0)
//  	    	{
 	    					
//  	    	}
//  	    	else
//  	    	{
// 	 	    	e.preventDefault();
// 	 	    	var param = document.getElementById('slDistrict').value;
	 	     
// 			         $.ajax({
// 			              url: '${pageContext.request.contextPath}/getsubdistrict',
// 			              type: 'POST',
// 			              data: {districtid : param},
// 			              dataType: 'json'
// 			         })
// 			         .done(function(data){
// 			              console.log(data);
	 				
// 	 				             //for json data type
// 	 				      for (var i in data) {
// 	 				        var obj = data[i];
// 	 				        var index = 0;
// 	 				        var key, val;
// 	 				        for (var prop in obj) {
// 	 				            switch (index++) {
// 	 				                case 0:
// 	 				                    key = obj[prop];
// 	 				                    break;
// 	 				                case 1:
// 	 				                    val = obj[prop];
// 	 				                    break;
// 	 				                default:
// 	 				                    break;
// 	 				            }
// 	 				        }
	 				        
// 	 				        $('#slSubDistrict').append("<option value=\"" + key + "\">" + val + "</option>");
	 				        
// 	 				    }
	 				             
// 	 				         })
// 	 				         .fail(function(){
// 	 				        	 console.log('Service call failed!');
// 	 				         });
//  	    	}
		         
//  	    });
//  	});
 	
 	
 	//sama saja sebenarnya dengan function slSubDistrictClick di atas, cuman ini untuk load subdistrict saat mau update merchant
 	function FuncSlSubDistrictClick(lDistrictID) {
 		var param = lDistrictID;
	     
        $.ajax({
             url: '${pageContext.request.contextPath}/getsubdistrict',
             type: 'POST',
             data: {districtid : param},
             dataType: 'json'
        })
        .done(function(data){
             console.log(data);
		
		             //for json data type
		      for (var i in data) {
		        var obj = data[i];
		        var index = 0;
		        var key, val;
		        for (var prop in obj) {
		            switch (index++) {
		                case 0:
		                    key = obj[prop];
		                    break;
		                case 1:
		                    val = obj[prop];
		                    break;
		                default:
		                    break;
		            }
		        }
		        
		        $('#slSubDistrict').append("<option value=\"" + key + "\">" + val + "</option>");
		        
		    }
		             
		         })
		         .fail(function(){
		        	 console.log('Service call failed!');
		         });
 	}
 	</script>
	
</body>
</html>