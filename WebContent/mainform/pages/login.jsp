<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"  import="javax.servlet.Servlet,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>GG Merchant | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="mainform/plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<form action="${pageContext.request.contextPath}/login" method="post">

<div class="login-box">
  <div class="login-logo">
  <img src="mainform/image/GPOINT_launcher_xxxhdpi.png" alt="Mountain View" style="width:130px;height:100px;"><br>
  </div>
  <!-- /.login-logo -->
  
  <div class="login-box-body">
  
  <c:if test="${condition == 1}">
      <div class="alert alert-danger alert-dismissible">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
         <h4><i class="icon fa fa-ban"></i> Sign In Failed !</h4>
      </div>
      </c:if>
      
  	 	<div class="input-group" style="left:-20px">
          <span class="input-group-addon"><img src="mainform/image/320X320 XXXDPI_username.png" width="40"></span>
          <input type="text" class="form-control" placeholder="USERNAME" style="height:92px; width:295px;" id="username" name="username">
        </div>
        <br>
        <div class="input-group" style="left:-20px">
          <span class="input-group-addon"><img src="mainform/image/320X320 XXXDPI_password.png" width="40"></span>
          <input type="password" class="form-control" placeholder="PASSWORD" style="height:92px; width:295px;" id="password" name="password">
        </div>
        <br><br>
        <button type="submit" class="btn btn-lg btn-block btn-gg" id="btnLogin" name="btnLogin">SIGN&nbsp&nbspIN</button>
  </div>
  <!-- /.login-box-body -->
  
</div>
<!-- /.login-box -->
</form>
<!-- jQuery 2.2.3 -->
<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="mainform/plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>

</body>
</html>
