<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="icon" href="mainform/image/GPOINT_launcher_mdpi.png">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>GG Merchant | Role Product</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="mainform/bootstrap/css/bootstrap.min.css">
<!-- Select2 -->
  <link rel="stylesheet" href="mainform/plugins/select2/select2.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Theme style -->
<link rel="stylesheet" href="mainform/dist/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="mainform/dist/css/skins/_all-skins.min.css">

<!-- DataTables -->
<link rel="stylesheet"
	href="mainform/plugins/datatables/dataTables.bootstrap.css">
	
<!-- Enable scroll for modal after showing another modal -->
<style type="text/css">	
#ModalUpdateInsert { overflow-y:scroll }
</style>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">

	<%@ include file="/mainform/pages/master_header.jsp"%>

	<form id="roleproduct" name="roleproduct"
		action="${pageContext.request.contextPath}/roleproduct" method="post">

		<div class="wrapper">
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<section class="content-header">
				<h1>
					<label style="color: #f6f6f6">Role Product</label>
				</h1>
				</section>

				<!-- Main content -->
				<section class="content">
				<div class="row">
					<div class="col-xs-12">

						<div class="box">

							<div class="box-body">
								<c:if test="${condition == 'SuccessInsert'}">
									<div class="alert alert-success alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-check"></i> Success
										</h4>
										Insert Role Product Success.
									</div>
								</c:if>

								<c:if test="${condition == 'FailedInsert'}">
									<div class="alert alert-danger alert-dismissible">
										<button type="button" class="close" data-dismiss="alert"
											aria-hidden="true">&times;</button>
										<h4>
											<i class="icon fa fa-ban"></i> Failed
										</h4>
										Insert Role Product Failed.
										<c:out value="${ErrorDescription}" />
										.
									</div>
								</c:if>



								<!-- modal update insert Role -->
								<div class="modal fade bs-example-modal-lg"
									id="ModalUpdateInsert" tabindex="-1" role="dialog"
									aria-labelledby="myLargeModalLabel">
									<div class="modal-dialog modal-lg" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="exampleModalLabel">
													<label id="lblTitleModalUpdateInsert"
														name="lblTitleModalUpdateInsert"></label>
												</h4>
											</div>
											<div class="modal-body">
												<div class="row">
													<div id="dvRoleProductId" class="form-group col-sm-6">
														<label for="inputEmail3" class="control-label">Role
															Product Id :</label><label id="mrkRoleProductId"
															for="recipient-name" class="control-label"><small>*</small></label>

														<input class="form-control" id="txtRoleProductId" name="txtRoleProductId">

													</div>

													<div id="dvRoleProductName" class="form-group col-sm-6">
														<label for="inputEmail3" class=" control-label">Role
															Product Name :</label><label id="mrkRoleProductName"
															for="recipient-name" class="control-label"><small>*</small></label>

														<input class="form-control" id="txtRoleProductName" name="txtRoleProductName">

													</div>
													
													<div id="dvSelectBrand" class="form-group col-sm-12">
													<label for="inputEmail3" class=" control-label">BrandId :</label>
													<select id="slBrand" class="form-control" style="width: 100%;">
													<c:forEach items="${listbrand}" var="brand">
                  										<option id="optBrand"
																value="<c:out value="${brand.brandID}" />">
																<li><a href="#"><c:out value="${brand.brandName}" /></a></li>
														</option>
													</c:forEach>
										             </select>
										             </div>
										             
													<div id="dvupdatemenu" name="dvupdatemenu">


														<!--Test Tab -->
														<div class="col-md-12">
															<!-- Custom Tabs -->
															<div class="nav-tabs-custom">
																<ul class="nav nav-tabs">
																	<li class="active"><a href="#tab_1"
																		data-toggle="tab">Merchant</a></li>
																	<li><a href="#tab_2" data-toggle="tab">Product</a></li>
																</ul>
																<div class="tab-content">
																	<div class="tab-pane active" id="tab_1">
																		<!--<< list merchant -->
																		<div class="col-md-4">

																			<div class="input-group">


																				<label for="lblMenu" class="control-label">Menu
																					Merchant :</label> <input id="txtSearchMerchant"
																					name="txtSearchMerchant" placeholder="Search"
																					onkeyup="filtermerchant()" type="text"
																					class="form-control"> <select multiple
																					id="slMerchant" name="slMerchant"
																					class="form-control" style="height: 350px">

																					<c:forEach items="${listmerchantmenu}"
																						var="menumerchant">
																						<option id="optMerchant"
																							value="<c:out value="${menumerchant.merchantID}" />">
																							<li><a href="#"><c:out
																										value="${menumerchant.merchantName}" /></a></li>
																						</option>
																					</c:forEach>

																				</select>

																			</div>

																		</div>

																		<div class="col-md-4 input-position text-center">
																		
																				<br> <br>
																				<button type="button"
																					class="btn btn-block btn-default"
																					onclick="FuncAddMenuMerchant()">Add ></button>
																				<button type="button"
																					class="btn btn-block btn-default"
																					onclick="FuncAddAllMenuMerchant()">Add All
																					>></button>
																				<button type="button"
																					class="btn btn-block btn-default"
																					onclick="FuncRemoveMenuMerchant()"><
																					Remove</button>
																				<button type="button"
																					class="btn btn-block btn-default"
																					onclick="FuncRemoveAllMenuMerchant()"><<
																					Remove All</button>
																		
																		</div>

																		<div class="col-md-4">
																			<div class="input-group">
																				<label for="lblAllowedMenuMerchant"
																					class="control-label">Allowed Menu Merchant
																					:</label> <select multiple id="slAllowedMerchant"
																					name="slAllowedMerchant" class="form-control"
																					style="height: 384px">
																					<c:forEach items="${listallowedmenumerchant}"
																						var="allowedmenumerchant">
																						<option
																							id="<c:out value="${allowedmenumerchant.merchantID}" />"
																							value="<c:out value="${allowedmenumerchant.merchantID}" />">
																							<c:out
																								value="${allowedmenumerchant.merchantName}" /></option>
																					</c:forEach>
																				</select>
																			</div>
																		</div>
																		<!--list merchant >> -->
																	</div>
																	<!-- /.tab-pane -->
																	<div class="tab-pane" id="tab_2">
																		<!--<< list Product -->
																		<div class="col-md-4">
																			<div class="input-group">
																				<label for="lblMenu" class="control-label">Menu
																					Product:</label> <input id="txtSearchProduct"
																					name="txtSearchProduct" placeholder="Search"
																					onkeyup="filterproduct()" type="text"
																					class="form-control"> <select multiple
																					id="slProduct" name="slProduct"
																					class="form-control select2" style="height: 350px">
																					<c:forEach items="${listproductmenu}"
																						var="menuproduct">
																						<option id="optProduct"
																							value="<c:out value="${menuproduct.productID}" />">
																							<c:out value="${menuproduct.productName}" /></option>
																					</c:forEach>
																				</select>
																			</div>

																		</div>

																		<div class="col-md-4 input-position text-center">
																			
																				<br> <br>
																				<button type="button"
																					class="btn btn-block btn-default"
																					onclick="FuncAddMenuProduct()">Add ></button>
																				<button type="button"
																					class="btn btn-block btn-default"
																					onclick="FuncAddAllMenuProduct()">Add All
																					>></button>
																				<button type="button"
																					class="btn btn-block btn-default"
																					onclick="FuncRemoveMenuProduct()">< Remove</button>
																				<button type="button"
																					class="btn btn-block btn-default"
																					onclick="FuncRemoveAllMenuProduct()"><<
																					Remove All</button>
																			
																		</div>

																		<div class="col-md-4">
																			<div class="input-group">
																				<label for="lblAllowedMenu" class="control-label">Allowed
																					Menu Product:</label>
																					<button type="button" class=" form-control btn btn-success" 
																						id="btnOpenModalPricePoint" name="btnOpenModalPricePoint"
																						onclick="OpenModalPricePoint()">Set Price & Point						
																				</button>
																					<select multiple
																					id="slAllowedProduct" name="slAllowedProduct"
																					class="form-control" style="height: 350px">
																					<c:forEach items="${listallowedmenu}"
																						var="allowedmenuproduct">
																						<option
																							id="<c:out value="${allowedmenuproduct.productID}" />"
																							value="<c:out value="${allowedmenuproduct.productID}" />">
																							<c:out value="${allowedmenuproduct.productName}" /></option>
																					</c:forEach>
																				</select>
																				
																			</div>
																		</div>
																		<!--list Product >> -->
																	</div>
																	<!-- /.tab-pane -->

																</div>
																<!-- /.tab-content -->
															</div>
															<!-- nav-tabs-custom -->
														</div>
														<!-- /.col -->
														<!--Test Tab -->




													</div>
												</div>
											</div>


											<div class="modal-footer">
												<div class="col-md-12">
													<button type="button" id="btnSave" name="btnSave"
														class="btn btn-gg-small" onclick="FuncSaveRule()">Save
														Role Product</button>
													<button type="button" id="btnUpdate" name="btnUpdate"
														class="btn btn-gg-small" onclick="FuncUpdateRule()">Register
														Role Product</button>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- /.end modal update & Insert -->

								<!-- modal update insert Point Price -->
								<div class="modal fade" id="ModalUpdatePointPrice" tabindex="-1"
									role="dialog" aria-labelledby="exampleModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"
													aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
												<h4 class="modal-title" id="exampleModalLabel">
													<label id="lblTitleModalUpdateInsert"
														name="lblTitleModalUpdateInsert"></label>
												</h4>
											</div>
											<div class="modal-body">
												<div id="dvProductID" class="form-group col-xs-6">
													<div class="col-xs-6">
														<label for="recipient-name" class="control-label">Product
															ID :</label>
													</div>
													<div class="col-xs-6">
														<label id="lblProductID" name="lblProductID"></label>
													</div>
												</div>

												<div id="dvProductName" class="form-group col-xs-6">
													<div class="col-xs-6">
														<label for="recipient-name" class="control-label">Product
															Name :</label>
													</div>
													<div class="col-xs-6">
														<label id="lblProductName" name="lblProductName"></label>
													</div>
												</div>


												<div id="dvPrice" class="form-group col-xs-12 ">
													<label for="message-text" class="control-label">Price</label>
													<div class="input-group">
														<span class="input-group-addon">Rp</span> <input
															id="txtPrice" name="txtPrice" type="number"
															class="form-control"> <span
															class="input-group-addon">,00</span>
													</div>
												</div>
												<div class="form-group col-xs-12 input-position text-center">
												<a  class="btn btn-app" onclick='funcConvertPoint()'>
                									<i class="fa fa-arrow-circle-down"></i> Convert
             									</a>
												</div>
												<div id="dvPoint" class="form-group col-xs-12">
													<label for="message-text" class="control-label">Point</label>
													<input class="form-control" id="txtPoint"
														name="txtPoint" readonly>
												</div>



												<div class="row"></div>
											</div>

											<div class="modal-footer">
												<button type="button" class="btn btn-gg-small"
													id="btnUpdate" name="btnUpdate"
													onclick="FuncValEmptyInput('updatepricepoint')">Update Price
													& Point</button>
											</div>
										</div>
									</div>
								</div>
								<!-- /.end modal Point Price -->

								<!--modal Delete -->
								<div class="example-modal">
									<div class="modal modal-danger" id="ModalDelete" tabindex="-1"
										role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
													<h4 class="modal-title">Alert Delete Role Product</h4>
												</div>
												<div class="modal-body">
													<input type="hidden" id="temp_txtRoleProductID"
														name="temp_txtRoleProductID"> <input type="hidden"
														id="temp_txtMerchantID" name="txtMerchantID"> <input
														type="hidden" id="temp_txtProductID"
														name="temp_txtProductID">
													<p>Are you sure to delete this Role Product document ?</p>
												</div>
												<div class="modal-footer">
													<button type="button" class="btn btn-outline pull-left"
														data-dismiss="modal">Close</button>
													<button type="submit" id="btnDelete" name="btnDelete"
														class="btn btn-outline">Delete</button>
												</div>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
									<!-- /.modal -->
								</div>
								<!--modal Delete -->

								<div class="col-xs-12">
									<button id="btnModalNew" name="btnModalNew" type="button"
										class="btn btn-gg-small pull-right" data-toggle="modal"
										data-target="#ModalUpdateInsert">
										<i class="fa fa-plus-circle"></i> ADD NEW
									</button>
								</div>
								<div class="col-xs-12">
									<br>
								</div>
								<div class="col-xs-12">
									<table id="tb_roleproduct"
										class="table table-bordered table-striped table-hover">
										<thead style="color: #7d0d08;">
											<!-- background-color: #d2d6de; -->
											<tr>
												<th>Role Product ID</th>
												<th>Role Product Name</th>
												<th>Brand ID</th>
												<th></th>
											</tr>
										</thead>

										<tbody>

											<c:forEach items="${listroleproduct}" var="roleproduct">
												<tr>
													<td><a href=""
														style="color: #144471; font-weight: bold;"
														data-toggle="modal" data-target="#ModalUpdateInsert"
														data-lroleproductid='<c:out value="${roleproduct.roleProductID}" />'
														data-lroleproductname='<c:out value="${roleproduct.roleProductName}" />'
														data-lbrandid='<c:out value="${roleproduct.brandID}" />'
														data-lkey="Update"> <c:out
																value="${roleproduct.roleProductID}" />
													</a></td>
													<td><c:out value="${roleproduct.roleProductName}" /></td>
													<td><c:out value="${roleproduct.brandID}" /></td>
													<td><a href="" data-toggle="modal"
														data-target="#ModalDelete"
														data-lroleproductid='<c:out value="${roleproduct.roleProductID}" />'>Delete</a></td>
												</tr>

											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
					<!-- /.col-xs-12 -->
				</div>
				<!-- /.row --> </section>
				<!-- /.content -->
			</div>
			<!-- /.content-wrapper -->

			<%@ include file="/mainform/pages/master_footer.jsp"%>
		</div>
		<!-- ./wrapper -->

	</form>

	<!-- jQuery 2.2.3 -->
	<script src="mainform/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<!-- Bootstrap 3.3.6 -->
	<script src="mainform/bootstrap/js/bootstrap.min.js"></script>
	<!-- Select2 -->
	<script src="mainform/plugins/select2/select2.full.min.js"></script>
	<!-- FastClick -->
	<script src="mainform/plugins/fastclick/fastclick.js"></script>
	<!-- AdminLTE App -->
	<script src="mainform/dist/js/app.min.js"></script>
	<!-- Sparkline -->
	<script src="mainform/plugins/sparkline/jquery.sparkline.min.js"></script>
	<!-- jvectormap -->
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script
		src="mainform/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
	<!-- SlimScroll 1.3.0 -->
	<script src="mainform/plugins/slimScroll/jquery.slimscroll.min.js"></script>
	<!-- ChartJS 1.0.1 -->
	<script src="mainform/plugins/chartjs/Chart.min.js"></script>
	<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
	<script src="mainform/dist/js/pages/dashboard2.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="mainform/dist/js/demo.js"></script>
	<!-- DataTables -->
	<script src="mainform/plugins/datatables/jquery.dataTables.min.js"></script>
	<script src="mainform/plugins/datatables/dataTables.bootstrap.min.js"></script>
	<!-- MouseTrap for adding shortcut key in this page -->
	<script src="mainform/plugins/mousetrap.js"></script>

	<script>
		$(function() {

// 			$("#slBrand").select2();
			$("slBrand option[0]").attr("selected", "selected");
			$("#tb_roleproduct").DataTable({
				"scrollX" : true
			});
			$('#mroleproduct').addClass('active');
			//shortcut for button 'new'
			Mousetrap.bind('n', function() {
				FuncButtonNew(), $('#ModalUpdateInsert').modal('show')
			});
		});

// 		$('#slAllowedProduct').on("click","option",
// 						function() {
//		}  ini kodingan untuk select on click

		$('#ModalUpdateInsert')
				.on(
						'shown.bs.modal',
						function(event) {
							FuncClear();

							var button = $(event.relatedTarget);
							var lRoleProductID = button.data('lroleproductid');
							var lRoleProductName = button.data('lroleproductname');
							var lbrandid = button.data('lbrandid');
							var lKey = button.data('lkey');
							var modal = $(this);
							$('#btnSave').show();
							$('#btnUpdate').hide();
							$("#txtRoleProductId").prop('disabled', false);
							$("#txtRoleProductName").prop('disabled', false);
							$("#slBrand").prop('disabled', false);
							$('#dvupdatemenu').hide();

							modal.find(".modal-body #txtRoleProductId").val(
									lRoleProductID);
							modal.find(".modal-body #txtRoleProductName").val(
									lRoleProductName);
							
							$("#slBrand option[id="+lbrandid+"]").prop("selected", "selected");
							
							$('#slMerchant option').prop('selected',
									true);
							$("#slMerchant option:selected").remove();

							$('#slProduct option')
									.prop('selected', true);
							$("#slProduct option:selected").remove();
							
							$('#slAllowedMerchant option').prop('selected',
									true);
							$("#slAllowedMerchant option:selected").remove();

							$('#slAllowedProduct option')
									.prop('selected', true);
							$("#slAllowedProduct option:selected").remove();

							if (lKey == "Update") {
								$('#dvupdatemenu').show();
								$('#btnSave').hide();
								$('#btnUpdate').show();
								$("#txtRoleProductId").prop('disabled', true);
								$("#slBrand").prop('disabled', true);

								//<<Javascript For 4 json
								$.ajax(
												{
													url : '${pageContext.request.contextPath}/getlistroleproduct',
													type : 'POST',
													data : {
														roleproductid :lRoleProductID,
														command : lKey,
														slbrand : lbrandid
													},
													dataType : 'json'
												})
										.done(
												function(data) {
													console.log(data);
													var data1 = data[0];
													var data2 = data[1];
													var data3 = data[2];
													var data4 = data[3];
													
													//<<fill Select Allow Merchant 
													for ( var i in data1) {
														var obj = data1[i];
														var index = 0;
														var id, name;
														for ( var prop in obj) {
															switch (index++) {
															case 0:
																id = obj[prop];
																break;
															case 1:
																name = obj[prop];
																break;
															default:
																break;
															}
														}
														if ($("#slAllowedMerchant option[value="
																+ id + "]").length > 0) {
														} else {
															$(
																	'#slAllowedMerchant')
																	.append(
																			"<option id=\"" + id + "\" value=\"" + id + "\">"
																					+ name
																					+ "</option>");
														}
													}
													//>>
													
													//<<fill Select Allow Product
													for ( var i in data2) {
														var obj = data2[i];
														var index = 0;
														var id, name;
														for ( var prop in obj) {
															switch (index++) {
															case 0:
																id = obj[prop];
																break;
															case 1:
																name = obj[prop];
																break;
															default:
																break;
															}
														}
														
														if ($("#slAllowedProduct option[value="+ id + "]").length > 0) 
														{
														} else {
															$(
																	'#slAllowedProduct')
																	.append(
																			"<option id=\"" + id + "\" value=\"" + id + "\">"
																					+ name
																					+ "</option>");
														}
													}
													//>>
													
													//<<fill Select Merchant 
													for ( var i in data3) {
														var obj = data3[i];
														var index = 0;
														var id, name;
														for ( var prop in obj) {
															switch (index++) {
															case 0:
																id = obj[prop];
																break;
															case 1:
																name = obj[prop];
																break;
															default:
																break;
															}
														}
														if ($("#slMerchant option[value="
																+ id + "]").length > 0) {
														} else {
															$('#slMerchant').append(
																			"<option id=\"" + id + "\" value=\"" + id + "\">"
																					+ name
																					+ "</option>");
														}
													}
													//>>
													
													//<<fill Select Product 
													for ( var i in data4) {
														var obj = data4[i];
														var index = 0;
														var id, name;
														for ( var prop in obj) {
															switch (index++) {
															case 0:
																id = obj[prop];
																break;
															case 1:
																name = obj[prop];
																break;
															default:
																break;
															}
														}
														if ($("#slProduct option[value="
																+ id + "]").length > 0) {
														} else {
															$('#slProduct').append(
																			"<option id=\"" + id + "\" value=\"" + id + "\">"
																					+ name
																					+ "</option>");
														}
													}
													//>>
												})
										.fail(
												function() {
													console
															.log('Service call failed!');
												})
							}
						})

		$('#ModalDelete').on('show.bs.modal', function(event) {
			var button = $(event.relatedTarget);
			var lroleproductid = button.data('lroleproductid');
			$("#temp_txtRoleProductID").val(lroleproductid);
		})
		
		function funcConvertPoint(){
			var price = $("#txtPrice").val();
			var slbrand = document.getElementById('slBrand').value;
			
			//Get Point
			$.ajax(
					{
						url : '${pageContext.request.contextPath}/getlistroleproduct',
						type : 'POST',
						data : {
							price : price,
							slbrand : slbrand,
							command : 'ConvertPoint'
						},
						dataType : 'json'
					})
			.done(
					function(data) {
						console.log(data);
						var data1 = data[0];
						//Check Disini bener atau tidak
						if (data.RoleProductID != "") {
							$("#txtPoint").val(data["Point"]);
							
						} else {
							alert("Failed Convert Point, Please Check Log Exception ...");
						}

					}).fail(function() {
				console.log('Service call failed!');
			})
			
		}
		
		function OpenModalPricePoint(){
			$("#txtPrice").val("");
			$("#txtPoint").val(""); 
			var lProductid = $('#slAllowedProduct option:selected').attr('id');
			var lProductname = $("#slAllowedProduct option:selected").text();
			var lRoleProductID = $("#txtRoleProductId").val();
			
			$("#lblProductName").text(lProductname);
			$("#lblProductID").text(lProductid);
			//<<check ProductID registered or not
			$
					.ajax(
							{
								url : '${pageContext.request.contextPath}/getlistroleproduct',
								type : 'POST',
								data : {
									roleproductid : lRoleProductID,
									productid : lProductid,
									command : 'CheckRegisteredProduct'
								},
								dataType : 'json'
							})
					.done(
							function(data) {
								console.log(data);
								var data1 = data[0];
								//Check Disini bener atau tidak
								if (data.RoleProductID != "") {
									$("#ModalUpdatePointPrice").modal('show');
									fillPricePoint(lRoleProductID,lProductid);
									
								} else {
									alert("This Product Id Not Registered, Please Registered First...");
								}

							}).fail(function() {
						console.log('Service call failed!');
					})
			//>>

		}
		
		function fillPricePoint(lRoleProductID, lProductid) {
			//<<Menampilkan price dan point
			$.ajax(
			{
				url : '${pageContext.request.contextPath}/getlistroleproduct',
				type : 'POST',
				data : {
					roleproductid : lRoleProductID,
					productid : lProductid,
					command : 'GetPricePoint'
				},
				dataType : 'json'
			})
			.done(
				function(data) {
					console.log(data);															
					$("#txtPrice").val(data["Price"]);
					$("#txtPoint").val(data["Point"]);
			}).fail(function() {
				console.log('Service call failed!');
			})
			//>>
		
		}
		
		function filtermerchant() {
			var keyword = document.getElementById("txtSearchMerchant").value;
			var select = document.getElementById("slMerchant");
			for (var i = 0; i < select.length; i++) {
				var txt = select.options[i].text;
				if (txt.substring(0, keyword.length).toLowerCase() !== keyword
						.toLowerCase()
						&& keyword.trim() !== "") {
					$(select.options[i]).attr('disabled', 'disabled').hide();
				} else {
					$(select.options[i]).removeAttr('disabled').show();
				}
			}
		}

		function filterproduct() {
			var keyword = document.getElementById("txtSearchProduct").value;
			var select = document.getElementById("slProduct");
			for (var i = 0; i < select.length; i++) {
				var txt = select.options[i].text;
				if (txt.substring(0, keyword.length).toLowerCase() !== keyword
						.toLowerCase()
						&& keyword.trim() !== "") {
					$(select.options[i]).attr('disabled', 'disabled').hide();
				} else {
					$(select.options[i]).removeAttr('disabled').show();
				}
			}
		}

		function FuncClear() {
			$('#mrkRoleProductId').hide();
			$('#mrkRoleProductName').hide();

			$('#dvRoleProductId').removeClass('has-error');
			$('#dvRoleProductName').removeClass('has-error');
		}

		function FuncSaveRule() {
			var txtRoleProductId = document.getElementById('txtRoleProductId').value;
			var txtRoleProductName = document.getElementById('txtRoleProductName').value;
			var slBrand = document.getElementById('slBrand').value;
		
			FuncClear();

			if (!txtRoleProductId.match(/\S/)) {
				$("#txtRoleProductId").focus();
				$('#dvRoleProductId').addClass('has-error');
				$('#mrkRoleProductId').show();
				return false;
			}

			if (!txtRoleProductName.match(/\S/)) {
				$("#txtRoleProductName").focus();
				$('#dvRoleProductName').addClass('has-error');
				$('#mrkRoleProductName').show();
				return false;
			}

			jQuery.ajax({
				url : '${pageContext.request.contextPath}/roleproduct',
				type : 'POST',
				dataType : 'text',
				data : {
					"key" : 'saveroleproduct',
					"txtRoleProductId" : txtRoleProductId,
					"txtRoleProductName" : txtRoleProductName,
					"slBrand" : slBrand
				},
				success : function(data, textStatus, jqXHR) {
					var url = '${pageContext.request.contextPath}/roleproduct';
					$(location).attr('href', url);
				},
				error : function(data, textStatus, jqXHR) {
					console.log('Service call failed!');
				}
			});
			return true;
		}

		function FuncUpdateRule() {
			var listAllowedMenuMerchant = document
					.getElementById('slAllowedMerchant');
			var listAllowedMenuProduct = document
					.getElementById('slAllowedProduct');
			var optionAllMenuMerchant = new Array();
			var optionAllMenuProduct = new Array();

			var optionVal = new Array();

			var txtRoleProductId = document.getElementById('txtRoleProductId').value;
			var txtRoleProductName = document.getElementById('txtRoleProductName').value;
		

			for (x = 0; x < listAllowedMenuMerchant.length; x++) {
				optionAllMenuMerchant
						.push(listAllowedMenuMerchant.options[x].value);
			}

			for (i = 0; i < listAllowedMenuProduct.length; i++) {
				optionAllMenuProduct
						.push(listAllowedMenuProduct.options[i].value);
			}

			jQuery.ajax({
				url : '${pageContext.request.contextPath}/roleproduct',
				type : 'POST',
				dataType : 'text',
				data : {
					"key" : 'updaterule',
					"listAllowedMerchant" : optionAllMenuMerchant,
					"listAllowedProduct" : optionAllMenuProduct,
					"txtRoleProductId" : txtRoleProductId,
					"txtRoleProductName" : txtRoleProductName
				},
				success : function(data, textStatus, jqXHR) {
					var url = '${pageContext.request.contextPath}/roleproduct';
					$(location).attr('href', url);
				},
				error : function(data, textStatus, jqXHR) {
					console.log('Service call failed!');
				}
			});
			
			
			return true;

		}

		//<< function for button Merchant --//
		function FuncAddMenuMerchant() {
			var MerchantID = document.getElementById('slMerchant').value;
			var Command = "AddMerchant";

			$.ajax({
				url : '${pageContext.request.contextPath}/getlistroleproduct',
				type : 'POST',
				data : {
					merchantid : MerchantID,
					command : Command
				},
				dataType : 'json'
			})
					.done(
							function(data) {
								console.log(data);
								for ( var i in data) {
									var obj = data[i];
									var index = 0;
									var id, name;
									for ( var prop in obj) {
										switch (index++) {
										case 0:
											id = obj[prop];
											break;
										case 1:
											name = obj[prop];
											break;

										default:
											break;
										}
									}

									if ($("#slAllowedMerchant option[value="
											+ id + "]").length > 0) {
									} else {
										$('#slAllowedMerchant').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">"
														+ name + "</option>");
									}

								}

							}).fail(function() {
						console.log('Service call failed!');
					})
		}

		function FuncAddAllMenuMerchant() {
			var MerchantID = "All";
			var Command = "AddMerchant";
			var slbrand = document.getElementById('slBrand').value;

			$.ajax({
				url : '${pageContext.request.contextPath}/getlistroleproduct',
				type : 'POST',
				data : {
					merchantid : MerchantID,
					command : Command,
					slbrand : slbrand
				},
				dataType : 'json'
			})
					.done(
							function(data) {
								console.log(data);
								for ( var i in data) {
									var obj = data[i];
									var index = 0;
									var id, name;
									for ( var prop in obj) {
										switch (index++) {
										case 0:
											id = obj[prop];
											break;
										case 1:
											name = obj[prop];
											break;

										default:
											break;
										}
									}

									if ($("#slAllowedMerchant option[value="
											+ id + "]").length > 0) {
									} else {
										$('#slAllowedMerchant').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">"
														+ name + "</option>");
									}

								}

							}).fail(function() {
						console.log('Service call failed!');
					})
		}

		function FuncRemoveMenuMerchant() {
			var MerchantID = document.getElementById('slAllowedMerchant').value;
			$("#slAllowedMerchant option[value=" + MerchantID + "]").remove();
		}

		function FuncRemoveAllMenuMerchant() {
			$('#slAllowedMerchant option').prop('selected', true);
			$("#slAllowedMerchant option:selected").remove();
		}
		// function for button Merchant >>//

		//<< function for button Product --//
		function FuncAddMenuProduct() {
			var ProductID = document.getElementById('slProduct').value;
			var Command = "AddProduct";
			

			$.ajax({
				url : '${pageContext.request.contextPath}/getlistroleproduct',
				type : 'POST',
				data : {
					productid : ProductID,
					command : Command
				},
				dataType : 'json'
			})
					.done(
							function(data) {
								console.log(data);
								for ( var i in data) {
									var obj = data[i];
									var index = 0;
									var id, name;
									for ( var prop in obj) {
										switch (index++) {
										case 0:
											id = obj[prop];
											break;
										case 1:
											name = obj[prop];
											break;

										default:
											break;
										}
									}

									if ($("#slAllowedProduct option[value="
											+ id + "]").length > 0) {
									} else {
										$('#slAllowedProduct').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">"
														+ name + "</option>");
									}

								}

							}).fail(function() {
						console.log('Service call failed!');
					})
		}

		function FuncAddAllMenuProduct() {
			var ProductID = "All";
			var Command = "AddProduct";
			var slbrand = document.getElementById('slBrand').value;

			$.ajax({
				url : '${pageContext.request.contextPath}/getlistroleproduct',
				type : 'POST',
				data : {
					productid : ProductID,
					command : Command,
					slbrand : slbrand
				},
				dataType : 'json'
			})
					.done(
							function(data) {
								console.log(data);
								for ( var i in data) {
									var obj = data[i];
									var index = 0;
									var id, name;
									for ( var prop in obj) {
										switch (index++) {
										case 0:
											id = obj[prop];
											break;
										case 1:
											name = obj[prop];
											break;

										default:
											break;
										}
									}

									if ($("#slAllowedProduct option[value="
											+ id + "]").length > 0) {
									} else {
										$('#slAllowedProduct').append(
												"<option id=\"" + id + "\" value=\"" + id + "\">"
														+ name + "</option>");
									}

								}

							}).fail(function() {
						console.log('Service call failed!');
					})
		}

		function FuncRemoveMenuProduct() {
			var ProductID = document.getElementById('slAllowedProduct').value;
			$("#slAllowedProduct option[value=" + ProductID + "]").remove();
		}

		function FuncRemoveAllMenuProduct() {
			$('#slAllowedProduct option').prop('selected', true);
			$("#slAllowedProduct option:selected").remove();
		}
		// function for button Product >>//
		
		function FuncValEmptyInput(lParambtn) {
			var txtRoleProductId = document.getElementById('txtRoleProductId').value;
			var lblProductID = $("#lblProductID").text();
			var txtPrice = document.getElementById('txtPrice').value;
			var txtPoint = document.getElementById('txtPoint').value;

			FuncClear();
			
			//<<update 
			$
					.ajax(
							{
								url : '${pageContext.request.contextPath}/getlistroleproduct',
								type : 'POST',
								data : {
									roleproductid : txtRoleProductId,
									productid : lblProductID,
									price : txtPrice,
									point : txtPoint,
									command : lParambtn
								},
								dataType : 'json'
							})
					.done(
							function(data) {
								console.log(data);								
								//Check Disini bener atau tidak
								if (data['Title'] == "SuccessUpdate") {
									$("#ModalUpdatePointPrice").modal('hide');
									//DO thing in this
								} else {
									alert("Failed Update Price & Point, Please Check Log Exception....");
								}

							}).fail(function() {
						console.log('Service call failed!');
					})
			//>>

			return true;
		}
	</script>
</body>
</html>